def total_animaux(troupeau):
    cpt_animaux = 0
    for animau in troupeau.values():
        cpt_animaux += animau
    return cpt_animaux

def tous_les_animaux(troupeau):
    ensemble_animaux = set()
    for animau in troupeau.keys():
        ensemble_animaux.add(animau)
    if len(ensemble_animaux) == 0:
        return None
    return ensemble_animaux

def specialise(troupeau):
    troupeau_animaux = False
    for animau in troupeau.values():
        if animau >= 30:
            return True
    return troupeau_animaux

def le_plus_represente(troupeau):
    nb_max_animaux = 0
    nom_animaux = None
    for (animau, nombre) in troupeau.items():
        if nom_animaux is None or nb_max_animaux < nombre:
            nb_max_animaux = nombre
            nom_animaux = animau
    return nom_animaux

def quantite_suffisante(troupeau):
    animaux_suffisante = True
    for animau in troupeau.values():
        if animau < 5:
            return False
    return animaux_suffisante

def reunion_revisions(troupeau1, troupeau2):
    reunion_animaux = dict()
    for (animau, nombre) in troupeau1.items():
        reunion_animaux[animau] = nombre
    for (animau, nombre) in troupeau2.items():
        if animau not in reunion_animaux:
            reunion_animaux[animau] = 0
        reunion_animaux[animau] += nombre
    return reunion_animaux