import super_heros


def test_moyenne_intelligence():
    avengers1 = {'Spiderman':(5, 5, 'araignée a quatre pattes'),
                  'Hulk':(7, 4, "Grand homme vert"),
                  'M becker':(2, 6, 'expert en graphe'),
                 'Agent 13':(2, 3, 'agent 13')
                }
    
    avengers2 = {'Iron Man':(7, 10, 'Homme en armure'),
                 'Thor': (9, 6, 'Roi de Asgard'),
                  'Captain America': (8, 7, 'Super soldat'),
                  'Black Widow': (7, 8, 'Agent du shiel')
                 }
    
    assert super_heros.moyenne_intelligence(avengers1) == 4.5
    assert super_heros.moyenne_intelligence(avengers2) == 7.75
    
    
def test_kikelplusfort():
    avengers1 = {'Spiderman':(5, 5, 'araignée a quatre pattes'),
                  'Hulk':(7, 4, "Grand homme vert"),
                  'M becker':(2, 6, 'expert en graphe'),
                 'Agent 13':(2, 3, 'agent 13')
                }
    
    avengers2 = {'Iron Man':(7, 10, 'Homme en armure'),
                 'Thor': (9, 6, 'Roi de Asgard'),
                  'Captain America': (8, 7, 'Super soldat'),
                  'Black Widow': (7, 8, 'Agent du shiel')
                 }
    
    assert super_heros.kikelplusfort(avengers1) == 'Hulk'
    assert super_heros.kikelplusfort(avengers2) == 'Thor'


def test_combienDeCretins():
    avengers1 = {'Spiderman':(5, 5, 'araignée a quatre pattes'),
                  'Hulk':(7, 4, "Grand homme vert"),
                  'M becker':(2, 6, 'expert en graphe'),
                 'Agent 13':(2, 3, 'agent 13')
                }
    
    avengers2 = {'Iron Man':(7, 10, 'Homme en armure'),
                 'Thor': (9, 6, 'Roi de Asgard'),
                  'Captain America': (8, 7, 'Super soldat'),
                  'Black Widow': (7, 8, 'Agent du shiel')
                 }
    
    assert super_heros.combienDeCretins(avengers1) == 2
    assert super_heros.combienDeCretins(avengers2) == 2