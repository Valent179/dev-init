import revisions


def test_total_animaux():
    troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
    troupeau_vide = dict()
    troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
    mon_troupeau = {'vache' :15, 'chevre' :115, 'mouton' : 100}
    assert revisions.total_animaux(troupeau_de_perrette) == 63
    assert revisions.total_animaux(troupeau_de_jean) == 32
    assert revisions.total_animaux(troupeau_vide) == 0
    assert revisions.total_animaux(mon_troupeau) == 230



def test_tous_les_animaux():
    jean = {'vache':12, 'cochon':17, 'veau':3}
    vide = dict()
    perrette = {'veau':14, 'vache':7, 'poule':42}
    mon_troupeau = {'vache' :15, 'chevre' :115, 'mouton' : 100} 
    assert revisions.tous_les_animaux(perrette) == {'veau', 'vache', 'poule'}
    assert revisions.tous_les_animaux(jean) == {'veau', 'vache', 'cochon'}
    assert revisions.tous_les_animaux(vide) is None
    assert revisions.tous_les_animaux(mon_troupeau) == {'vache', 'chevre', 'mouton'}


def test_specialise():
    troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
    troupeau_vide = dict()
    troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
    mon_troupeau = {'vache' :15, 'chevre' :115, 'mouton' : 100}  
    assert revisions.specialise(troupeau_de_perrette)
    assert not revisions.specialise(troupeau_de_jean)
    assert not revisions.specialise(troupeau_vide)
    assert revisions.specialise(mon_troupeau) 


def test_quantite_suffisante():
    troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
    troupeau_vide = dict()
    troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
    mon_troupeau = {'vache' :15, 'chevre' :115, 'mouton' : 100}   
    assert revisions.quantite_suffisante(troupeau_de_perrette)
    assert not revisions.quantite_suffisante(troupeau_de_jean)
    assert revisions.quantite_suffisante(troupeau_vide)
    assert revisions.quantite_suffisante(mon_troupeau)


def test_le_plus_represente():
    troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
    troupeau_vide = dict()
    troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
    mon_troupeau = {'vache' :15, 'chevre' :115, 'mouton' : 100}   
    assert revisions.le_plus_represente(troupeau_de_perrette) == 'poule'
    assert revisions.le_plus_represente(troupeau_de_jean) == "cochon"
    assert revisions.le_plus_represente(troupeau_vide) is None
    assert revisions.le_plus_represente(mon_troupeau) == 'chevre'


def test_reunion_revisions():
    troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
    troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
    troupeau_vide = dict()    
    mon_troupeau = {'vache' :15, 'chevre' :115, 'mouton' : 100}
    assert revisions.reunion_revisions(troupeau_de_perrette, troupeau_vide) == troupeau_de_perrette
    assert revisions.reunion_revisions(troupeau_vide, troupeau_de_jean) == troupeau_de_jean
    assert revisions.reunion_revisions(troupeau_de_perrette, troupeau_de_jean) == {'vache':12+7, 'cochon':17, 'veau':3+14, 'poule':42}
    assert troupeau_de_jean == {'vache':12, 'cochon':17, 'veau':3}
    assert troupeau_de_perrette == {'veau':14, 'vache':7, 'poule':42}
    assert revisions.reunion_revisions(troupeau_de_perrette, mon_troupeau) == {'veau':14, 'vache':7+15, 'poule':42, 'chevre':115, 'mouton': 100}
    assert revisions.reunion_revisions(mon_troupeau, troupeau_de_jean) == {'vache':15+12, 'chevre':115, 'mouton':100, 'cochon':17, 'veau':3}
    assert troupeau_de_jean == {'vache':12, 'cochon':17, 'veau':3}
    assert troupeau_de_perrette == {'veau':14, 'vache':7, 'poule':42}

