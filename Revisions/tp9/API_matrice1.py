def matrice ( nb_lignes , nb_colonnes , valeur_par_defaut ):
    liste_valeur = []
    for val in range(nb_lignes * nb_colonnes):
        liste_valeur.append(valeur_par_defaut)
    return liste_valeur

def get_nb_lignes(matrice):
    return matrice[0]

def get_nb_colonnes(matrice):
    return matrice[1]

def set_val ( matrice , ligne , colonne , nouvelle_valeur ):
    (lig, col, val) = matrice
    val[ligne * col + colonne] = nouvelle_valeur