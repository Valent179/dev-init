def toutes_les_familles(pokedex):
    ensemble_pokemon = set()
    for (_, type_pok) in pokedex:
        ensemble_pokemon.add(type_pok)
    return ensemble_pokemon

def nombre_pokemons(pokedex, famille):
    cpt_type_pokemon = 0
    for (_, type_pokemon) in pokedex:
        if type_pokemon == famille:
            cpt_type_pokemon += 1
    return cpt_type_pokemon

def frequences_famille(pokedex):
    dict_famille = dict()
    for (_, type_pokemon) in pokedex:
        if type_pokemon not in dict_famille.keys():
            dict_famille[type_pokemon] = 0
        dict_famille[type_pokemon] += 1
    return dict_famille

def dico_par_famille(pokedex):
    dict_famille = dict()
    for (nom_pok, type_pokemon) in pokedex:
        if type_pokemon not in dict_famille:
            dict_famille[type_pokemon] = set()
        dict_famille[type_pokemon].add(nom_pok)
    return dict_famille

def famille_la_plus_representee(pokedex):
    plus_represente = frequences_famille(pokedex)
    nb_plus_represente = 0
    nom_plus_represente = None
    for (nom_pok, nb_pok) in plus_represente.items():
        if nb_pok > nb_plus_represente or nom_plus_represente is None:
            nb_plus_represente = nb_pok
            nom_plus_represente = nom_pok
    return nom_plus_represente