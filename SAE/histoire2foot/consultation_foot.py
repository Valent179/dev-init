import histoire2foot

# Ici vos fonctions dédiées aux interactions

# ici votre programme principal
def creation_menu(titre, liste_options):
    """Affiche un menu à partir du titre et d'une liste_options

    Args:
        titre (str): nom du titre
        liste_options (list): liste d'option
    """
    print("+" + "-"*len(titre) + "+")
    print("|" + titre + "|")
    print("+" + "-"*len(titre) + "+")
    for nom_option in range(1, len(liste_options)+1 ):
        print(str(nom_option) + "-> " + liste_options[nom_option - 1])

def demander_nombre(message, borne_max):
    """Retourne le nombre de l'utilisateur s'il eest compris dans l'intervalle

    Args:
        message (str): message de l'utilisateur
        borne_max (int): nombre max de la borne
    """
    res = input(message)
    if res.isdecimal():
        res = int(res)
        if res >= 1 and res <= borne_max:
            return res
    return None

def menu(titre, liste_options):
    """Donne le numéro de l'option menu choisie par l'utilisateur

    Args:
        titre (str): titre du menu
        liste_options ([type]): liste d'option du menu
    """
    creation_menu(titre, liste_options)
    res = demander_nombre("Quelle est votre nombre ? ", len(liste_options))
    return res


def programme_principal():
    liste_options = ["Nom d'équipe favorite",  
                    "Autre équipe et ces résultats de ces matchs", 
                    "Les matchs déroulés dans une ville", 
                    "Buts moyen d'une comtétion", "Equipe favorie et ces stats", 
                    "Fusionner deux fichier de matchs",
                    "Changer de fichier", "Quitter le menu footix"
                    ]
    liste_matchs = []
    nom_fic = input(" Quelle est votre fichier ? ")
    liste_matchs = histoire2foot.charger_matchs(nom_fic)
    while True:
        rep = menu("Menu du footix", liste_options)
        if rep is None:
            print("Cette option n'existe pas.")
        elif rep == 1: # permet de demander le nom de son équipe favorite dans un fichier 
            equipe_pref = input("Quelle est votre équipe préféré ? ")
            while equipe_pref not in histoire2foot.liste_des_equipes(liste_matchs):
                print("Vous équipe n'est pas dans cette liste. Veuillez en choissir un autre. ")
                equipe_pref = input("Quelle est votre équipe préféré ? ")
            print("Votre équipe favorite est " + str(equipe_pref) + " est bien dans la liste des équipes.")
        elif rep == 2: #donne les stats d'une autre équipe prit en paramètre
            nom_equipe = input("Nom de votre équipe ")
            res_equipe = histoire2foot.resultats_equipe(liste_matchs, nom_equipe)
            print("Les résultats des matchs de " + str(nom_equipe) + " sont de " + str(res_equipe[0]) + " victoires, elle a " + str(res_equipe[1]) + " matchs nuls et a " + str(res_equipe[2]) + " défaites.")
        elif rep == 3: #donne le nombre de matchs qui se sont déroulés dans une ville
            ville = input("Quelle est le nom de la ville ? ")
            ville_match = histoire2foot.matchs_ville(liste_matchs, ville)
            if ville_match == []:
                print("Il n'y a pas de matchs qui se sont déroulés dans cette ville pour ce fichier. ")
            else:
                print("Le nombre de matchs qui se sont déroulés dans cette ville sont de " + str(ville_match))
        elif rep == 4: #donne la moyenne de buts dans une compétition
            competition = input("Quelle compétition voulez_vous ? ")
            but_moyen_competition = histoire2foot.nombre_moyen_buts(liste_matchs, competition)
            if but_moyen_competition == 0:
                print("Il n'y a pas eu de buts dans cette compétition, ou surement, il n'y a pas de donné de cette compétition dans ce fichier")
            else:
                print("Le nombre de buts moyens de cette compétition est de " + str(but_moyen_competition))
        elif rep == 5: #donne les stats de l'équipe favorite choisi en paramètre plutôt
            res_equipe = histoire2foot.resultats_equipe(liste_matchs, equipe_pref) #  résultats de l'équipe favorite
            print("Votre équipe a fait " + str(res_equipe[0]) + " victoires, elle a " + str(res_equipe[1]) + " matchs nuls et a " + str(res_equipe[2]) + " défaites")
            date_victoire = histoire2foot.premiere_victoire(liste_matchs, equipe_pref) # date de la première victoire de l'équipe favorite
            if date_victoire is None: # si il n'y a pas de date pour cette équipe 
                print("Elle n'a jamais eu de victoire répertorier dans ce fichier")
            else: # il y a une date pour cette équipe
                print("La victoire de : " + str(equipe_pref) + " date du " + str(date_victoire))
            equipe_meilleur = histoire2foot.meilleures_equipes(liste_matchs) # si l'équipe favorite fait partie des meilleurs équipes
            if equipe_meilleur == []: # ne fait pas parti des meilleurs équipes
                print("Votre équipe ne fait pas parti des meilleurs équipes dans ce fichier. ")
            else: # fait parti des meilleurs équipent
                print("Votre équipe fait parti des meilleurs équipes.")
            equipe_sans_defaite = histoire2foot.nb_matchs_sans_defaites(liste_matchs, equipe_pref) # donne le nombre de matchs snas défaite de cette équipe favorite
            print("Votre équipe a fait " + str(equipe_sans_defaite) + " matchs sans défaite. ")
        elif rep == 6: # demande si on veut fusionner deux listes
            fic_fusionner1 = input("Quelle est le premier fichier que tu veux fusionner ? ")
            fic_fusionner2 = input("Quelle est le deuxième fichier que tu veux fusionner ? ")
            fic_fusionner = histoire2foot.fusionner_matchs(fic_fusionner1, fic_fusionner2)
            if fic_fusionner != []: # si la fusion ne donne rien
                print("Vos deux listes sont bien fusionner. ")
        elif rep == 7: # permet de demander à l'utilisateur de changer de liste
            nom_fic = input(" Quelle est votre fichier ? ")
            liste_matchs = histoire2foot.charger_matchs(nom_fic)
        else:
            print("Vos deux listes sont bien fusionner. ")
            break
    print("Merci de votre visite sur le menu du footix. Au revoir et à bientôt. ")


programme_principal()