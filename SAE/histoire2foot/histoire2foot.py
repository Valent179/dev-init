"""Fichier source de la SAE 1.01 partie 1
Historique des matchs de football internationaux
"""

# ---------------------------------------------------------------------------------------------
# Exemples de données pour vous aidez à faire des tests
# ---------------------------------------------------------------------------------------------
    
# exemples de matchs de foot
match1=('2021-06-28', 'France', 'Switzerland', 3, 3, 'UEFA Euro', 'Bucharest', 'Romania', True)
match2=('1998-07-12', 'France', 'Brazil', 3, 0, 'FIFA World Cup', 'Saint-Denis', 'France', False)
match3=('1978-04-05', 'Germany', 'Brazil', 0, 1, 'Friendly', 'Hamburg', 'Germany', False)

#exemples de liste de matchs de foot
liste1=[('1970-04-08', 'France', 'Bulgaria', 1, 1, 'Friendly', 'Rouen', 'France', False), 
        ('1970-04-28', 'France', 'Romania', 2, 0, 'Friendly', 'Reims', 'France', False), 
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False), 
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
liste2=[('1901-03-09', 'England', 'Northern Ireland', 3, 0, 'British Championship', 'Southampton', 'England', False), 
        ('1901-03-18', 'England', 'Wales', 6, 0, 'British Championship', 'Newcastle', 'England', False), 
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False), 
        ('1902-05-03', 'England', 'Scotland', 2, 2, 'British Championship', 'Birmingham', 'England', False), 
        ('1903-02-14', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Wolverhampton', 'England', False), 
        ('1903-03-02', 'England', 'Wales', 2, 1, 'British Championship', 'Portsmouth', 'England', False), 
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False), 
        ('1905-02-25', 'England', 'Northern Ireland', 1, 1, 'British Championship', 'Middlesbrough', 'England', False), 
        ('1905-03-27', 'England', 'Wales', 3, 1, 'British Championship', 'Liverpool', 'England', False), 
        ('1905-04-01', 'England', 'Scotland', 1, 0, 'British Championship', 'London', 'England', False), 
        ('1907-02-16', 'England', 'Northern Ireland', 1, 0, 'British Championship', 'Liverpool', 'England', False), 
        ('1907-03-18', 'England', 'Wales', 1, 1, 'British Championship', 'London', 'England', False), 
        ('1907-04-06', 'England', 'Scotland', 1, 1, 'British Championship', 'Newcastle', 'England', False), 
        ('1909-02-13', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Bradford', 'England', False), 
        ('1909-03-15', 'England', 'Wales', 2, 0, 'British Championship', 'Nottingham', 'England', False), 
        ('1909-04-03', 'England', 'Scotland', 2, 0, 'British Championship', 'London', 'England', False)
        ]
liste3=[('1901-03-30', 'Belgium', 'France', 1, 2, 'Friendly', 'Bruxelles', 'Belgium', False),
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False),
        ('1903-04-04', 'Brazil', 'Argentina', 3, 0, 'Friendly', 'Sao Paulo', 'Brazil', False),
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False), 
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False), 
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
liste4=[('1978-03-19', 'Argentina', 'Peru', 2, 1, 'Copa Ramón Castilla', 'Buenos Aires', 'Argentina', False), 
        ('1978-03-29', 'Argentina', 'Bulgaria', 3, 1, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-04-05', 'Argentina', 'Romania', 2, 0, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-05-03', 'Argentina', 'Uruguay', 3, 0, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-01', 'Germany', 'Poland', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-02', 'Argentina', 'Hungary', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-02', 'France', 'Italy', 1, 2, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-02', 'Mexico', 'Tunisia', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-03', 'Austria', 'Spain', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-03', 'Brazil', 'Sweden', 1, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-03', 'Iran', 'Netherlands', 0, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-03', 'Peru', 'Scotland', 3, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-06', 'Argentina', 'France', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-06', 'Germany', 'Mexico', 6, 0, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-06', 'Hungary', 'Italy', 1, 3, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-06', 'Poland', 'Tunisia', 1, 0, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-07', 'Austria', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-07', 'Brazil', 'Spain', 0, 0, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-07', 'Iran', 'Scotland', 1, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-07', 'Netherlands', 'Peru', 0, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-10', 'Argentina', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-10', 'France', 'Hungary', 3, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-10', 'Germany', 'Poland', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-11', 'Austria', 'Brazil', 0, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-11', 'Iran', 'Peru', 1, 4, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-11', 'Netherlands', 'Scotland', 2, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-11', 'Spain', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-14', 'Argentina', 'Poland', 2, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-14', 'Austria', 'Netherlands', 1, 5, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-14', 'Brazil', 'Peru', 3, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-14', 'Germany', 'Italy', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-18', 'Argentina', 'Brazil', 0, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-18', 'Austria', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-18', 'Germany', 'Netherlands', 2, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-18', 'Peru', 'Poland', 0, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-21', 'Argentina', 'Peru', 6, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-21', 'Austria', 'Germany', 3, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-21', 'Brazil', 'Poland', 3, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-21', 'Italy', 'Netherlands', 1, 2, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-24', 'Brazil', 'Italy', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-25', 'Argentina', 'Netherlands', 3, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False)
]

# -----------------------------------------------------------------------------------------------------
# listes des fonctions à implémenter
# -----------------------------------------------------------------------------------------------------

# Fonctions à implémenter dont les tests sont fournis


def equipe_gagnante(match):
    """retourne le nom de l'équipe qui a gagné le match. Si c'est un match nul on retourne None

    Args:
        match (tuple): un match

    Returns:
        str: le nom de l'équipe gagnante (ou None si match nul)
    """   
    if match[3] == match[4]: 
        resultat = None
    else:
        if match[3] > match[4]:
            resultat = match[1]
        else:
            resultat = match[2]
    return resultat



def victoire_a_domicile(match):
    """indique si le match correspond à une victoire à domicile

    Args:
        match (tuple): un match

    Returns:
        bool: True si le match ne se déroule pas en terrain neutre et que l'équipe qui reçoit a gagné
    """    
    if match[1] == match[7]:
        return True
    return False


def nb_buts_marques(match):
    """indique le nombre total de buts marqués lors de ce match

    Args:
        match (tuple): un match

    Returns:
        int: le nombre de buts du match 
    """ 
    somme_buts = match[3] + match[4]
    return somme_buts



def matchs_ville(liste_matchs, ville):
    """retourne la liste des matchs qui se sont déroulés dans une ville donnée
    
    Args:
        liste_matchs (list): une liste de matchs
        ville (str): le nom d'une ville

    Returns:
        list: la liste des matchs qui se sont déroulé dans la ville ville    
    """
    liste_match_ville = []
    for match in liste_matchs:
        if match[6] == ville:
            liste_match_ville.append(match)
    return liste_match_ville



def nombre_moyen_buts(liste_matchs, nom_competition):
    """retourne le nombre moyen de buts marqués par match pour une compétition donnée

    Args:
        liste_matchs (list): une liste de matchs
        nom_competition (str): le nom d'une compétition
    
    Returns:
        float: le nombre moyen de buts par match pour la compétition
    """
    somme_buts = 0
    moyenne_but = 0
    cpt_but = 0
    for but in liste_matchs:
        if but[5] == nom_competition:
            somme_buts += nb_buts_marques(but)
            cpt_but += 1
    if cpt_but != 0:
        moyenne_but = somme_buts / cpt_but
    return moyenne_but



def est_bien_trie(liste_matchs):
    """vérifie si une liste de matchs est bien trié dans l'ordre chronologique
       puis pour les matchs se déroulant le même jour, dans l'ordre alphabétique
       des équipes locales

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        bool: True si la liste est bien triée et False sinon
    """    
    match = 0
    for match in range(1,len(liste_matchs)):
        if liste_matchs[match][0] < liste_matchs[match-1][0]:
            return False
        else:
            if liste_matchs[match][0] == liste_matchs[match-1][0]:
                if liste_matchs[match][1] < liste_matchs[match-1][1]:
                    return False
    return True


def fusionner_matchs(liste_matchs1, liste_matchs2):
    """Fusionne deux listes de matchs triées sans doublons en une liste triée sans doublon
    sachant qu'un même match peut être présent dans les deux listes

    Args:
        liste_matchs1 (list): la première liste de matchs
        liste_matchs2 (list): la seconde liste de matchs

    Returns:
        list: la liste triée sans doublon comportant tous les matchs de liste_matchs1 et liste_matchs2
    """ 
    matchs1 = 0
    matchs2 = 0
    liste_fusion = []
    while matchs1 < len(liste_matchs1) and matchs2 < len(liste_matchs2):
        if liste_matchs1[matchs1] == liste_matchs2[matchs2]:
            liste_fusion.append(liste_matchs1[matchs1])
            matchs1 += 1
            matchs2 += 1
        else:
            if liste_matchs1[matchs1] < liste_matchs2[matchs2]:
                liste_fusion.append(liste_matchs1[matchs1])
                matchs1 += 1
            else:
                liste_fusion.append(liste_matchs2[matchs2])
                matchs2 += 1
    if matchs1 < len(liste_matchs1):
        for match in range(matchs1, len(liste_matchs1)):
            liste_fusion.append(liste_matchs1[match])
    if matchs2 < len(liste_matchs2):
        for match in range(matchs2, len(liste_matchs2)):
            liste_fusion.append(liste_matchs2[match])
    return liste_fusion
                


def resultats_equipe(liste_matchs, equipe):
    """donne le nombre de victoire, de matchs nuls et de défaites pour une équipe donnée

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        tuple: un triplet d'entiers contenant le nombre de victoires, nuls et défaites de l'équipe
    """    
    victoire_equipe = 0
    nul_equipe = 0
    defaite_equipe = 0
    for match in range(len(liste_matchs)):
        if liste_matchs[match][1] == equipe or liste_matchs[match][2] == equipe:
            res_match = equipe_gagnante(liste_matchs[match])
            if res_match is None:
                nul_equipe += 1
            else:
                if res_match == equipe:
                    victoire_equipe += 1
                else:
                    defaite_equipe += 1
    return (victoire_equipe, nul_equipe, defaite_equipe)


def plus_gros_scores(liste_matchs):
    """retourne la liste des matchs pour lesquels l'écart de buts entre le vainqueur et le perdant est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs avec le plus grand écart entre vainqueur et perdant
    """    
    ecart_match = 0
    ecart_max = None
    liste_ecart = []
    for ind_match in range(len(liste_matchs)):
        ecart_match = abs(liste_matchs[ind_match][3] - liste_matchs[ind_match][4])
        if ecart_max is None or ecart_match > ecart_max:
            ecart_max = ecart_match
            liste_ecart = []
            liste_ecart.append(liste_matchs[ind_match])
        else:
            if ecart_match == ecart_max:
                liste_ecart.append(liste_matchs[ind_match])
    return liste_ecart



def liste_des_equipes(liste_matchs):
    """retourne la liste des équipes qui ont participé aux matchs de la liste
    Attention on ne veut voir apparaitre le nom de chaque équipe qu'une seule fois

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: une liste de str contenant le noms des équipes ayant jouer des matchs
    """
    liste_participant = []
    for match in range(len(liste_matchs)):
        if liste_matchs[match][1] not in liste_participant:
            liste_participant.append(liste_matchs[match][1])
        if liste_matchs[match][2] not in liste_participant:
            liste_participant.append(liste_matchs[match][2])
    return  liste_participant


def premiere_victoire(liste_matchs, equipe):
    """retourne la date de la première victoire de l'equipe. Si l'equipe n'a jamais gagné de match on retourne None

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        str: la date de la première victoire de l'equipe
    """    
    for match in range(len(liste_matchs)):
        if liste_matchs[match][1] == equipe or liste_matchs[match][2] == equipe:
            res_match = equipe_gagnante(liste_matchs[match])
            if res_match == equipe:
                return liste_matchs[match][0]
    return None
            


def nb_matchs_sans_defaites(liste_matchs, equipe):
    """retourne le plus grand nombre de matchs consécutifs sans défaite pour une equipe donnée.

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        int: le plus grand nombre de matchs consécutifs sans défaite du pays nom_pays
    """
    max_sans_defaite = 0
    cpt_sans_defaite = 0
    for match in range(len(liste_matchs)):
        if liste_matchs[match][1] == equipe or liste_matchs[match][2] == equipe:
            res_match = equipe_gagnante(liste_matchs[match])
            if res_match == equipe or res_match is None:
                cpt_sans_defaite += 1
            else:
                if max_sans_defaite < cpt_sans_defaite:
                    max_sans_defaite = cpt_sans_defaite
                cpt_sans_defaite = 0
    if max_sans_defaite < cpt_sans_defaite:
         max_sans_defaite = cpt_sans_defaite
    return max_sans_defaite



def charger_matchs(nom_fichier):
    """charge un fichier de matchs donné au format CSV en une liste de matchs

    Args:
        nom_fichier (str): nom du fichier CSV contenant les matchs

    Returns:
        list: la liste des matchs du fichier
    """    
    liste_fic = []
    fic=open(nom_fichier, 'r', encoding="UTF-8")
    fic.readline()
    for ligne in fic:
        booleen = True
        ligne_fic = ligne.split(",")
        if ligne_fic[8][:-1] == 'False' or ligne_fic[8][:-1] == 'FALSE':
            booleen = False
        liste_fic.append((ligne_fic[0], ligne_fic[1], ligne_fic[2], int(ligne_fic[3]), int(ligne_fic[4]), ligne_fic[5], ligne_fic[6], ligne_fic[7], booleen))
    fic.close()
    return liste_fic



def sauver_matchs(liste_matchs,nom_fichier):
    """sauvegarde dans un fichier au format CSV une liste de matchs

    Args:
        liste_matchs (list): la liste des matchs à sauvegarder
        nom_fichier (str): nom du fichier CSV

    Returns:
        None: cette fonction ne retourne rien
    """    
    fic=open(nom_fichier, 'a')
    fic.write("date,home_team,away_team,home_score,away_score,tournament,city,country,neutral")
    for match in liste_matchs:
        fic.write(match + "\n")
    fic.close()
    


# Fonctions à implémenter dont il faut également implémenter les tests


def plus_de_victoires_que_defaites(liste_matchs, equipe):
    """vérifie si une équipe donnée a obtenu plus de victoires que de défaites
    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        bool: True si l'equipe a obtenu plus de victoires que de défaites
    """
    resultat_equipe = False
    for match in range(len(liste_matchs)):
        if liste_matchs[match][1] == equipe or liste_matchs[match][2] == equipe:
            res_equipe = resultats_equipe(liste_matchs, equipe)
            if res_equipe[0] > res_equipe[2]:
                resultat_equipe = True
    return resultat_equipe





def matchs_spectaculaires(liste_matchs):
    """retourne la liste des matchs les plus spectaculaires, c'est à dire les
    matchs dont le nombre total de buts marqués est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs les plus spectaculaires
    """
    cpt_but = 0
    max_buts = 0
    liste_equipe = []
    for match in range(len(liste_matchs)):
        cpt_but = liste_matchs[match][3] + liste_matchs[match][4]
        if cpt_but > max_buts:
            max_buts = cpt_but
            liste_equipe = []
            liste_equipe.append(liste_matchs[match])
        else:
            if cpt_but == max_buts:
                liste_equipe.append(liste_matchs[match])
    return liste_equipe
        


def meilleures_equipes(liste_matchs):
    """retourne la liste des équipes de la liste qui ont le plus petit nombre de defaites

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des équipes qui ont le plus petit nombre de defaites
    """
    liste_meilleur_equipe = []
    min_defaite = None
    for equipe in liste_des_equipes(liste_matchs):
        res_equipe = resultats_equipe(liste_matchs, equipe)
        if min_defaite is None or res_equipe[2] < min_defaite:
            liste_meilleur_equipe = []
            min_defaite = res_equipe[2]
            liste_meilleur_equipe.append(equipe)
        else:
            if res_equipe[2] == min_defaite:
                liste_meilleur_equipe.append(equipe)
    return liste_meilleur_equipe
