#Ex 5.1
def rangement_betement(liste_de_cadeaux):
    taille_cadeaux = 0
    malle = []
    traineau = []
    for (cadeau, taille) in liste_de_cadeaux:
        taille_cadeaux += taille
        if taille_cadeaux <= 50:
            malle.append((cadeau, taille))
        else:
            traineau.append(malle)
            malle = [(cadeau, taille)]
            taille_cadeaux = taille
    if len(malle) != 0:
        traineau.append(malle)
    return traineau

def test_rangement_betement():
    TRAIN = ('train', 18)
    NOURS = ('peluche', 47)
    VELO = ('velo', 24)
    STYLO = ('stylo', 2)
    CLAVIER = ('clavier', 25)
    CONSOLE = ('console', 5)
    TV = ('tv', 24)
    
    cadeaux_2021 = [TRAIN, NOURS, VELO, STYLO, CLAVIER, CONSOLE, TV]
    
    assert rangement_betement(cadeaux_2021) == [[('train', 18)], [('peluche', 47)], [('velo', 24), ('stylo', 2)],
                                                [('clavier', 25), ('console', 5)], [('tv', 24)]]
    


# Ex 5.2
def taille(cadeau):
    return cadeau[1]

def trier_cadeau(liste_de_cadeau):
    liste_cadeau = sorted(liste_de_cadeau, key = taille)
    return liste_cadeau

def ranger_cadeau(liste_de_cadeaux):
    malle = []
    traineau = []
    taille_cadeau = 0
    liste_cadeaux = trier_cadeau(liste_de_cadeaux)
    for (cadeau, quantite) in liste_cadeaux:
        taille_cadeau += quantite
        if taille_cadeau <= 50:
            malle.append((cadeau, quantite))
        else:
            traineau.append(malle)
            malle = [(cadeau, quantite)]
            taille_cadeau = quantite
    if len(malle) != 0:
        traineau.append(malle)
    return traineau

""" Autre version plus rapide
def ranger_cadeau(liste_de_cadeaux):
    liste_cadeaux = trier_cadeau(liste_de_cadeaux)
    traineau = rangement_betement(liste_cadeaux)
    return traineau
"""


def test_ranger_cadeau():
    TRAIN = ('train', 18)
    NOURS = ('peluche', 47)
    VELO = ('velo', 24)
    STYLO = ('stylo', 2)
    CLAVIER = ('clavier', 25)
    CONSOLE = ('console', 5)
    TV = ('tv', 24)
    
    cadeaux_2021 = [TRAIN, NOURS, VELO, STYLO, CLAVIER, CONSOLE, TV]
    
    assert ranger_cadeau(cadeaux_2021) == [[('stylo', 2), ('console', 5), ('train', 18), ('velo', 24)],
                                           [('tv', 24), ('clavier', 25)],
                                           [('peluche', 47)]]