"""
API qui permet de modéliser une partie de mastermind
Une partie de mastermind est caractérisée par :
 - un code secret = une combinaison à découvrir
 - la taille de la combinaison à découvrir
 - le nombre de "couleurs" utilisées dans la partie
 - le nombre de tentatives maximales pour découvrir le code secret
 - la liste des tentatives

Dans cette API, on décide de modéliser une partie de mastermind par un dictionnaire
"""

import combinaison_POO as combinaison

class Mastermind(object):
    def __init__(self, taille_code_secret, nb_couleurs, nb_tentatives):
        self._code_secret = combinaison.Combinaison(taille_code_secret)
        combinaison.genere(self._code_secret, nb_couleurs)
        self._taille_code_secret = taille_code_secret
        self._nb_couleurs = nb_couleurs
        self._nb_tentatives = nb_tentatives
        self._tentatives = []
        
    def get_taille_code_secret(self):
        return self._taille_code_secret
    
    def get_nb_couleurs(self):
        return self._nb_couleurs
    
    def get_nb_tentatives(self):
        return self._nb_tentatives
    
    def get_code_secret(self):
        return self._code_secret
    
    def get_numero_tentatives(self):
        return len(self._tentatives) + 1
    
    def get_une_tentative(self, numero):
        return self.get_nb_tentatives()[numero - 1]
    
    def affiche_jeu(self):
        """
    Permet d'afficher le jeu avec les différentes tentatives déjà effectuées
    Cette fonction ne renvoie rien
    """ 
        print("===========================\nHistorique :" )
        for numero in range(1, self.get_numero_tentatives()):
            (proposition, (nb_bien_places, nb_mal_places)) = self.get_une_tentative(numero)
            print("tentative", numero, ":", combinaison.to_string(proposition), nb_bien_places, "bien placés et", nb_mal_places, "mal placés")
        print("---------------")
        print("tentative numero ", self.get_numero_tentatives)
    
    def nouvelle_tentative(self):
        """
    Permet au joueur d'entrer une nouvelle tentative
    Renvoie un tuple contenant (le nombre de pions bien placés, le nombre de pions mal placés)
    """
        proposition = combinaison.Combinaison(self.get_taille_code_secret())
        combinaison.saisir_combinaison(proposition, self.get_nb_couleurs())
        res = combinaison.compare(proposition, self.get_code_secret())
        self._tentatives.append((proposition, res))
        return res


    def jouer(self):
        """permet de lancer une partie de mastermind"""
        fin_du_jeu = False
        while not fin_du_jeu:
            self.affiche_jeu()   
            (nb_bien_places, _) = self.nouvelle_tentative()
            if nb_bien_places == self.get_taille_code_secret():
                    print("===========================\nVous avez gagné en", self.get_numero_tentatives()-1, 'tentatives')
                    fin_du_jeu = True
            elif self.get_numero_tentatives() > self.get_nb_tentatives():
                print("===========================\nVous avez perdu")
                print("Il fallait trouver ", combinaison.to_string(self.get_code_secret()))
                fin_du_jeu = True
        print("Merci et à bientôt !")