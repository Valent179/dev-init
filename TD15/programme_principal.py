""" Programme principal qui permet de lancer une partie de mastermind """
import mastermind

une_partie = mastermind.creer_jeu(4, 6, 10)
mastermind.jouer(une_partie)