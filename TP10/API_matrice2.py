""" Matrices : API n 2 """

#Ex 3.1

def construit_matrice(nb_lignes, nb_colonnes, valeur_par_defaut = None):
    """crée une nouvelle matrice en mettant la valeur par défaut dans chacune de ses cases.

    Args:
        nb_lignes (int): le nombre de lignes de la matrice
        nb_colonnes (int): le nombre de colonnes de la matrice
        valeur_par_defaut : La valeur que prendra chacun des éléments de la matrice

    Returns:
        une nouvelle matrice qui contient la valeur par défaut dans chacune de ses cases
    """
    matrice = []
    for col in range(nb_lignes):
        matrice.append([valeur_par_defaut] * nb_colonnes)
    return matrice

#Ex 3.2

def get_nb_lignes(matrice):
    """permet de connaître le nombre de lignes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de lignes de la matrice
    """
    return len(matrice)


#Ex 3.3
def get_nb_colonnes(matrice):
    """permet de connaître le nombre de colonnes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de colonnes de la matrice
    """
    return len(matrice[0])

#Ex 3.4

def get_val(matrice , ligne , colonne):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case

    Returns:
        None
    """
    return matrice[ligne][colonne]


#Ex 3.5

def set_val(matrice , ligne , colonne , nouvelle_valeur):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case

    Returns:
        None
    """
    matrice[ligne][colonne] = nouvelle_valeur


#Ex 3.6

def get_ligne(matrice, ligne):
    """permet de renvoyer un ligne de la matrice

    Args:
        matrice (tuple): une matrice
        ligne (int): le nombre de la ligne

    Returns:
        int : la ligne de la matrice
    """
    lig = []
    for colonne in range(get_nb_colonnes(matrice)):
        lig.append(get_val(matrice, ligne, colonne))
    return lig

#Ex 3.7

def get_colonne(matrice, colonne):
    """permet de renvoyer un colonne de la matrice

    Args:
        matrice (tuple): une matrice
        colonne (int): le nombre de la colonne
    """   
    col = []
    for ligne in range(get_nb_lignes(matrice)):
        col.append(get_val(matrice, ligne, colonne))
    return col


def charge_matrice_str(nom_fichier):
    """permet créer une matrice de str à partir d'un fichier CSV.

    Args:
        nom_fichier (str): le nom d'un fichier CSV (séparateur  ',')

    Returns:
        une matrice de str
    """
    matrice = None
    fic = open(nom_fichier,'r')
    for ligne in fic:
        info = ligne.split(",")
        matrice = construit_matrice(int(info[0]), int(info[1]), None)
        for lig in range(int(info[0])):
            for col in range(int(info[1])):
                valeur = info[lig * int(info[1]) + col + 2]
                if valeur.isdigit():
                    valeur = int(valeur)
                set_val(matrice, lig, col, valeur)
    fic.close()
    return matrice


def sauve_matrice(matrice, nom_fichier):
    """permet sauvegarder une matrice dans un fichier CSV.
    Attention, avec cette fonction, on perd l'information sur le type des éléments

    Args:
        matrice : une matrice
        nom_fichier (str): le nom du fichier CSV que l'on veut créer (écraser)

    Returns:
        None
    """
    fic = open(nom_fichier, 'a')
    lignes = get_nb_lignes(matrice)
    colonnes = get_nb_colonnes(matrice)
    fic.write(str(lignes)+",")
    fic.write(str(colonnes)+",")
    for ligne in range(lignes):
        for colonne in range(colonnes):
            fic.write(str(get_val(matrice, ligne, colonne))+",")
    fic.write("\n")
    fic.close()




#Exercice 5.1

def get_diagonale_principale(matrice):
    """" Retourne la diagonale sous la forme d'une liste
    Args:
        matrice : une matrice
    Returns:    
        list: la liste de la diagonale
    """
    diagonale = []
    colonnes = get_nb_colonnes(matrice)
    lignes = get_nb_lignes(matrice)
    if colonnes != lignes:
        return None
    else:
        for ligne in range(lignes):
            for colonne in range(lignes):
                if ligne == colonne:
                    diagonale.append(get_val(matrice, ligne, colonne))
    return diagonale


#Ex 5.2

def get_diagonale_secondaire(matrice):
    """" Retourne la diagonale inverse sous la forme d'une liste
    Args:
        matrice : une matrice
    Returns:    
        list: la liste de la diagonale inverse
    """
    ligne = 0
    diagonale_inverse = []
    colonnes = get_nb_colonnes(matrice)
    lignes = get_nb_lignes(matrice)
    if colonnes != lignes:
        return None
    else:
        for colonne in range(colonnes-1, -1, -1):
            diagonale_inverse.append(get_val(matrice, ligne, colonne))
            ligne += 1
    return diagonale_inverse


#Ex 3.3

def transpose(matrice):
    """renvoie la matrice transposé

    Args:
        matrice : une matrice
    Returns:
        une matrice
    """