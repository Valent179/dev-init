def remove_all(liste, a_supprimer):
    ind = 0
    while ind < len(liste):
        if liste[ind] == a_supprimer:
            liste.pop(ind)
        else:
            ind += 1
            
def test_remove_all():
    liste_nb = ['A', 'B', 'B', 'C', 'A', 'B', 'E', 'B', 'B']
    remove_all(liste_nb, 'B')
    assert liste_nb == ['A', 'C', 'A', 'E']
