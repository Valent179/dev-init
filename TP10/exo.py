def syca(n):
    liste_syca = [n]
    while n > 1:
        if n % 2 == 0:
            n = n // 2
            liste_syca.append(n)
        else:
            n = 3 * n + 1
            liste_syca.append(n)
    return liste_syca

def test_syca():
    assert syca(2) == [2, 1]