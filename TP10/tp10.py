"""Init Dev : TP10"""

# =====================================================================
# Exercice 1 : Choix de modélisation et complexité
# =====================================================================
# Modélisation n°1
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v1 dans le fichier de tests

def appartient_v1(pokemon, pokedex): 
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for (nom_pokemon, _) in pokedex:
        if nom_pokemon == pokemon:
            return True
    return False


def toutes_les_attaques_v1(pokemon, pokedex): 
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    attaques_pokemon = set()
    for (nom, type_attaque) in pokedex:
        if nom in pokemon:
            attaques_pokemon.add(type_attaque)
    return attaques_pokemon


def nombre_de_v1(attaque, pokedex): 
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    cpt_pokemon = 0
    for (_, type_attaque) in pokedex:
        if type_attaque == attaque:
            cpt_pokemon += 1
    return cpt_pokemon


def attaque_preferee_v1(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    nb_attaque_preferee = 0
    pref_attaque = dict()
    max_attaque = None
    for (_, type_attaque) in pokedex:
        if type_attaque not in pref_attaque:
            pref_attaque[type_attaque] = 0
        pref_attaque[type_attaque] += 1
        if nb_attaque_preferee < pref_attaque[type_attaque]:
            nb_attaque_preferee = pref_attaque[type_attaque]
            max_attaque = type_attaque
    return max_attaque

# =====================================================================
# Modélisation n°2
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v2 dans le fichier de tests

def appartient_v2(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for nom_pokemon in pokedex.keys():
        if nom_pokemon == pokemon:
            return True
    return False


def toutes_les_attaques_v2(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    return pokedex[pokemon]


def nombre_de_v2(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    cpt_attaque = 0
    for nom_attaque in pokedex.values():
        for att in nom_attaque:
            if att == attaque:
                cpt_attaque += 1
    return cpt_attaque


def attaque_preferee_v2(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    nb_attaque_preferee = 0
    pref_attaque = dict()
    max_attaque = None
    for type_attaque in pokedex.values():
        for nom_attaque in type_attaque:
            if nom_attaque not in pref_attaque:
                pref_attaque[nom_attaque] = 0
            pref_attaque[nom_attaque] += 1
            if nb_attaque_preferee < pref_attaque[nom_attaque]:
                nb_attaque_preferee = pref_attaque[nom_attaque]
                max_attaque = nom_attaque
    return max_attaque

# =====================================================================
# Modélisation n°3
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v3 dans le fichier de tests


def appartient_v3(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for nom_pok in pokedex.values():
        if pokemon in nom_pok:
            return True
    return False


def toutes_les_attaques_v3(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    ensemble_attaque = set()
    for (type_attaque, nom_pokemon) in pokedex.items():
        if pokemon in nom_pokemon:
            ensemble_attaque.add(type_attaque)
    return ensemble_attaque


def nombre_de_v3(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    if attaque in pokedex:
        return len(pokedex[attaque])
    else:
        return 0


def attaque_preferee_v3(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    max_attaque = None
    nb_attaque_preferee = 0
    for attaque in pokedex:
        if nombre_de_v3(attaque, pokedex) > nb_attaque_preferee:
            nb_attaque_preferee = nombre_de_v3(attaque, pokedex)
            max_attaque = attaque
    return max_attaque
            

# =====================================================================
# Transformations
# =====================================================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1):
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    dict_pokedex2 = dict()
    for (nom_pokemon, type_attaque) in pokedex_v1:
        if nom_pokemon not in dict_pokedex2:
            dict_pokedex2[nom_pokemon] = set()
        dict_pokedex2[nom_pokemon].add(type_attaque)
    return dict_pokedex2


# Version 1 ==> Version 2

def v2_to_v3(pokedex_v2):
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    dict_pokedex3 = dict()
    for (nom_pokemon, type_attaque) in pokedex_v2.items():
        for attaque in type_attaque:
            if attaque not in dict_pokedex3:
                dict_pokedex3[attaque] = set()
            dict_pokedex3[attaque].add(nom_pokemon)
    return dict_pokedex3


# =====================================================================
# Exercice 2 : Ecosystème
# =====================================================================

def extinction_immediate(ecosysteme, animal):
    """
    renvoie True si animal s'éteint immédiatement dans l'écosystème faute
    de nourriture
    """
    if animal not in ecosysteme or ecosysteme[animal] is None:
        return None
    else:
        return not ecosysteme[animal] in ecosysteme


def en_voie_disparition(ecosysteme, animal):
    """
    renvoie True si animal s'éteint est voué à disparaitre à long terme
    """
    tour = 0
    extinction = False
    while tour < len(ecosysteme) and not extinction and not ecosysteme[animal] is None:
        if extinction_immediate(ecosysteme, animal):
            return True
        else:
            animal = ecosysteme[animal]
        tour += 1
    return extinction
            

def animaux_en_danger(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en danger d'extinction immédiate"""
    ensemble_extinction = set()
    for animal in ecosysteme:
        if extinction_immediate(ecosysteme, animal):
            ensemble_extinction.add(animal)
    return ensemble_extinction


def especes_en_voie_disparition(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en voués à disparaitre à long terme
    """
    ensemble_disparition = set()
    for animal in ecosysteme:
        if en_voie_disparition(ecosysteme, animal):
            ensemble_disparition.add(animal)
    return ensemble_disparition


# =====================================================================
# Exercice 4 : Ecosystème
# =====================================================================

def pokemons_par_famille(liste_pokemon):
    """retourne un dictionnaire de pokemons

    Args:
        liste_pokemon (list): liste de pokemons
    """    
    dict_pokemon = {}
    for (nom_pokemon,type,_) in liste_pokemon:
        for type_pokemon in type:
            if not type_pokemon in dict_pokemon:
                dict_pokemon[type_pokemon] = set()
            dict_pokemon[type_pokemon].add(nom_pokemon)
    return dict_pokemon