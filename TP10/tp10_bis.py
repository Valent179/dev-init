def v1_to_v2 ( pokedex_v1 ) :
    dict_pokemon = dict()
    for (nom_pokemon, attaque) in pokedex_v1:
        if nom_pokemon not in dict_pokemon:
            dict_pokemon[nom_pokemon] = set()
        dict_pokemon[nom_pokemon].add(attaque)
    return dict_pokemon

def v2_to_v3 ( pokedex_v2 ) :
    dict_pokemon = dict()
    for (nom_pokemon, attaque) in pokedex_v2.items():
        for nom_attaque in attaque:
            if nom_attaque not in dict_pokemon:
                dict_pokemon[nom_attaque] = set()
            dict_pokemon[nom_attaque].add(nom_pokemon)
    return dict_pokemon