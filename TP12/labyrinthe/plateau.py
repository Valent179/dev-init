"""
Permet de modéliser un le_plateau de jeu avec :
    - une matrice qui contient des nombres entiers
    - chaque nombre entier correspond à un item :
      MUR, COULOIR, PERSONNAGE, FANTOME
"""
import matrice

MUR = 1
COULOIR = 0
PERSONNAGE = 2
FANTOME = 3

NORD = 'z'
OUEST = 'q'
SUD = 'w'
EST = 's'


def init(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    labyrinthe = matrice.charge_matrice(nom_fichier)
    matrice.set_val(labyrinthe, 0, 0, PERSONNAGE)
    matrice.set_val(labyrinthe, matrice.get_nb_lignes(labyrinthe)-1, matrice.get_nb_colonnes(labyrinthe)-1, FANTOME)
    return labyrinthe

def est_sur_le_plateau(le_plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    sur_plateau = True
    colonne = matrice.get_nb_colonnes(le_plateau)
    ligne = matrice.get_nb_lignes(le_plateau)
    (no_ligne, no_colonne) = position
    if no_ligne >= ligne  or no_colonne >= colonne or no_ligne < 0 or no_colonne < 0:
        return False
    return sur_plateau


def get(le_plateau, position):
    """renvoie la valeur de la case qui se trouve à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        int: la valeur de la case qui se trouve à la position donnée ou
             None si la position n'est pas sur le plateau
    """
    (no_ligne, no_colonne) = position
    if est_sur_le_plateau(le_plateau, position):
        return matrice.get_val(le_plateau, no_ligne, no_colonne)
    return None


def est_un_mur(le_plateau, position):
    """détermine s'il y a un mur à la poistion donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        bool: True si la case à la position donnée est un MUR, False sinon
    """
    valeur_mur = False
    (no_ligne, no_colonne) = position
    if matrice.get_val(le_plateau, no_ligne, no_colonne) == MUR:
        return True
    return valeur_mur


def contient_fantome(le_plateau, position):
    """Détermine s'il y a un fantôme à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est un FANTOME, False sinon
    """
    valeur_fantome = False
    (no_ligne, no_colonne) = position
    if matrice.get_val(le_plateau, no_ligne, no_colonne) == FANTOME:
        return True
    return valeur_fantome


def est_la_sortie(le_plateau, position):
    """Détermine si la position donnée est la sortie
       cad la case en bas à droite du labyrinthe

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est la sortie, False sinon
    """
    valeur_sortie = False
    (no_ligne, no_colonne) = position
    ligne = matrice.get_nb_lignes(le_plateau)
    colonne = matrice.get_nb_colonnes(le_plateau)
    if ligne - 1 == no_ligne and colonne - 1 == no_colonne:
        return True
    return valeur_sortie



def deplace_personnage(le_plateau, personnage, direction):
    """déplace le PERSONNAGE sur le plateau si le déplacement est valide
       Le personnage ne peut pas sortir du plate
E       IndexError: list index out of rangennage sur le plateau
        direction (str): la direction de déplacement SUD, EST, NORD, OUEST

    Returns:
        [tuple]: la nouvelle position du personnage
    """
    (no_ligne_arr, no_colonne_arr) = personnage
    if direction == NORD:    
        coordonnes_personnage = (no_ligne_arr - 1, no_colonne_arr)
    elif direction == SUD:
        coordonnes_personnage = (no_ligne_arr + 1, no_colonne_arr)
    elif direction == OUEST:
        coordonnes_personnage = (no_ligne_arr, no_colonne_arr - 1)
    elif direction == EST:
        coordonnes_personnage = (no_ligne_arr, no_colonne_arr + 1)
    if est_sur_le_plateau(le_plateau, coordonnes_personnage):
        if not est_un_mur(le_plateau, coordonnes_personnage):
            (no_ligne_personnage, no_colonne_personnage) = coordonnes_personnage
            matrice.set_val(le_plateau, no_ligne_personnage, no_colonne_personnage, PERSONNAGE)
            matrice.set_val(le_plateau, no_ligne_arr, no_colonne_arr, COULOIR)
            return coordonnes_personnage
    return personnage



def voisins(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    (no_ligne, no_colonne) = position
    set_voisins = set()
    if est_sur_le_plateau(le_plateau, (no_ligne+1, no_colonne)):
        if not est_un_mur(le_plateau, (no_ligne+1, no_colonne)):
            set_voisins.add((no_ligne+1, no_colonne))
    if est_sur_le_plateau(le_plateau, (no_ligne-1, no_colonne)):
        if not est_un_mur(le_plateau, (no_ligne-1, no_colonne)):
            set_voisins.add((no_ligne-1, no_colonne))
    if est_sur_le_plateau(le_plateau, (no_ligne, no_colonne+1)):
        if not est_un_mur(le_plateau, (no_ligne, no_colonne+1)):
            set_voisins.add((no_ligne, no_colonne+1))
    if est_sur_le_plateau(le_plateau, (no_ligne, no_colonne-1)):
        if not est_un_mur(le_plateau, (no_ligne, no_colonne-1)):
            set_voisins.add((no_ligne, no_colonne-1))
    return set_voisins



def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
       
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        matrice: une matrice qui a la taille du plateau dont la case qui se trouve à la
       position_de_depart est à 0 les autres cases contiennent la longueur du
       plus court chemin pour y arriver (les murs et les cases innaccessibles sont à None)
    """
    nb_lignes = matrice.get_nb_lignes(le_plateau)
    nb_colonnes = matrice.get_nb_colonnes(le_plateau)
    (no_ligne, no_colonne) = position_depart
    creer_matrice = matrice.new_matrice(nb_lignes, nb_colonnes, None)
    matrice.set_val(creer_matrice, no_ligne, no_colonne, 0)
    iteration = 0
    while iteration < 2:
        iteration += 1
        for ligne in range(nb_lignes):
            for colonne in range(nb_colonnes):
                if not est_un_mur(le_plateau, (ligne, colonne)):
                    if matrice.get_val(creer_matrice, ligne, colonne) is None:
                        est_voisins = voisins(le_plateau, (ligne, colonne))
                        for (lig, col) in est_voisins:
                            valeur = matrice.get_val(creer_matrice, lig, col)
                            if valeur != None:
                                matrice.set_val(creer_matrice, ligne, colonne, valeur+1)
                                iteration = 0
    return creer_matrice

le_plateau = init()
matrice.affiche(fabrique_le_calque(le_plateau, (0, 0)))

def fabrique_chemin(le_plateau, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    calque = fabrique_le_calque(le_plateau, position_depart)
    chemin = []
    position_actuelle = position_arrivee
    while position_actuelle != position_depart:
        chemin.append(position_actuelle)
        plus_petit = None
        for (ligne, colonne) in voisins(le_plateau, position_actuelle):
            valeur = matrice.get_val(calque, ligne, colonne)
            if valeur != None:
                if plus_petit is None or valeur < plus_petit:
                    plus_petit = valeur
                    position_actuelle = (ligne, colonne)
    return chemin


def deplace_fantome(le_plateau, fantome, personnage):
    """déplace le FANTOME sur le plateau vers le personnage en prenant le chemin le plus court

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    chemin = fabrique_chemin(le_plateau, fantome, personnage)
    if chemin != []:
        (ligne, colonne) = fantome
        matrice.set_val(le_plateau, ligne, colonne,  0)
        (ligne, colonne) = chemin[-1]
        matrice.set_val(le_plateau, ligne, colonne, 3)
        return chemin[-1]
    return fantome
