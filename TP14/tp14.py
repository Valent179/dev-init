"""TP 14"""

# EXERCICE 1

def recette_la_plus_facile(menu):
    """
    Résultat : le nom de la recette qui est la plus facile
    
    """
    def recette_facile(tuple):
        return menu[tuple][1]
    if len(menu) != 0:
        min = sorted(menu, key = recette_facile)
        return min[0]
    return None
    


def temps_total_de_preparation(menu):
    """
    Résultat : renvoie le temps total de préparation des recettes du menu
    """
    somme_preparation = 0
    for (temps, _) in menu.values():
        somme_preparation += temps
    return somme_preparation

def menu_de_chef(menu):
    """
    Résultat : vérifie si le menu contient au moins une recette
    difficile (difficulté > 5)
    
    """
    recette_difficile = False
    for (_, difficulter) in menu.values():
        if difficulter > 5:
            return True
    return recette_difficile


def recette_la_plus_longue(menu):
    """
    Résultat : le nom de la recette qui demande le plus de temps
    de préparation et cuisson
    
    """
    def recette_longue(tuple):
        return menu[tuple][0]
    if len(menu) != 0:
        max_temps = sorted(menu, key = recette_longue)
        return max_temps[len(menu) - 1]
    return None


def recettes_triee_par_temps(menu):
    """
    Résultat : la liste des noms de recettes triées par ordre croissant
    de temps de préparation    
    """
    def triee_temps(tuple):
        return menu[tuple][0]
    return sorted(menu, key = triee_temps)
        

def recettes_triee_par_difficulte(menu):
    """
    Résultat : la liste des noms de recettes triées par ordre croissant
    de difficulté 
    """
    def triee_difficulte(tuple):
        return menu[tuple][1]
    return sorted(menu, key = triee_difficulte)


# EXERCICE 2

def reglement(prix, porte_monnaie):
    liste_argent = []
    porte_monnaie_decroissant = sorted(porte_monnaie, reverse = True)
    somme_porte_monnaie = 0
    for nombre in porte_monnaie:
        somme_porte_monnaie += nombre
    if somme_porte_monnaie >= prix:
        for nombre in porte_monnaie_decroissant:
            new_prix = prix - nombre
            if new_prix >= 0:
                prix = new_prix
                liste_argent.append(nombre)
            if prix == 0:
                return liste_argent
    return None



# EXERCICE 3

def duellistes(dresseurs):
    """
    parametre: dresseurs est un dictionnaires :
    - clé : le nom du dresseur (str)
    - valeur : son classement Elo (int)
    résultat : renvoie le nom des deux dresseurs qui ont les classements
    Elo les plus proches
    """
    ...
