# Exercice 1

def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max = 0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle = 0 #longueur du plateau actuel
    for ind in range(len(chaine)):
        if chaine[ind] == chaine[ind-1]: # si la lettre actuelle est égale à la précédente
            lg_actuelle += 1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle > lg_max:
                lg_max = lg_actuelle
            lg_actuelle = 1
    if lg_actuelle > lg_max: #cas du dernier plateau
        lg_max = lg_actuelle
    return lg_max


def test_plus_long_plateau():
    assert plus_long_plateau('aaaaa') == 5
    assert plus_long_plateau('') == 0
    assert plus_long_plateau('11132315645646555555') == 6
    assert plus_long_plateau('rrrrrrrrrrrrrrrrrrrrrrrr') == 24


test_plus_long_plateau()

#Exercice 2

def ville_la_plus_peuplee(liste_villes,liste_popu):
    """Donne la ville étant le plus peuplée

    Args:
        liste_villes : liste de villes
        liste_popu : liste de nombres de population

    Returns:
        str : La ville étant le plus peuplée
    """    
    if len(liste_villes) == 0 or len(liste_popu) == 0:
        res = None
    else:
        popu = 0
        for ind in range(len(liste_popu)):
            if liste_popu[ind] > liste_popu[popu]:
                popu = ind
        res = liste_villes[popu]
    return res

def test_ville_la_plus_peuplee():
    liste_villes = ["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
    population = [45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]
    assert ville_la_plus_peuplee([],[]) == None
    assert ville_la_plus_peuplee(['Blois'],[45871]) == 'Blois'
    assert ville_la_plus_peuplee(liste_villes,population) == 'Tours'
    assert ville_la_plus_peuplee(['fdbjdb'],[]) == None

test_ville_la_plus_peuplee()
# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]


# Exercice 3

def transformer_chaine_nombre_entier(chaine):
    """Transforme une chaine de caractère en un nombre entier

    Args:
        chaine (str): chaine de caractère composer de nombre

    Returns:
        int : retourne un nombre entier
    """    

    nombre_entier = 0
    if chaine == '':
        nombre_entier = None
    else:
        for indice in range(len(chaine)):
            if chaine[indice] == '0':
                nombre_entier += 0*10**(len(chaine)-indice-1)
            if chaine[indice] == '1':
                nombre_entier += 1*10**(len(chaine)-indice-1)
            if chaine[indice] == '2':
                nombre_entier += 2*10**(len(chaine)-indice-1)
            if chaine[indice] == '3':
                nombre_entier += 3*10**(len(chaine)-indice-1)
            if chaine[indice] == '4':
                nombre_entier += 4*10**(len(chaine)-indice-1)
            if chaine[indice] == '5':
                nombre_entier += 5*10**(len(chaine)-indice-1)
            if chaine[indice] == '6':
                nombre_entier += 6*10**(len(chaine)-indice-1)
            if chaine[indice] == '7':
                nombre_entier += 7*10**(len(chaine)-indice-1)
            if chaine[indice] == '8':
                nombre_entier += 8*10**(len(chaine)-indice-1)
            if chaine[indice] == '9':
                nombre_entier += 9*10**(len(chaine)-indice-1)                        
    return nombre_entier

def test_transformer_chaine_nombre_entier():
    assert transformer_chaine_nombre_entier('2021') == 2021
    assert transformer_chaine_nombre_entier('') == None
    assert transformer_chaine_nombre_entier('0') == 0
    assert transformer_chaine_nombre_entier('111') == 111

test_transformer_chaine_nombre_entier()


#Exercice 4

def rechercher_mots(liste_mot,lettre):
    """Donne les mots commençant par une certaine lettre dans la liste de mots

    Args:
        liste (list): liste de mots
        lettre (str) : lettre choisi

    Returns :
        list : liste des mots commençant par une certaine lettre
    """    
    liste_mots = []
    if liste_mot != []:
        for mots in liste_mot:
            if mots != '':
                if mots[0] == lettre :
                    liste_mots.append(mots)
    return liste_mots

def test_rechercher_mots():
    assert rechercher_mots(["salut","hello","hallo","ciao","hola"],"h") == ["hello","hallo","hola"]
    assert rechercher_mots(["salut","hello","hallo","ciao","hola"],"a") == []
    assert rechercher_mots(["salut","hello","hallo","ciao","hola"],"s") == ["salut"]
    assert rechercher_mots([],'c') == []
    assert rechercher_mots(["","a","bb"],'b') == ["bb"]

test_rechercher_mots()

#Exercice 5

def decoupage_mots(phrase):
    """Donne la liste de mots à partir de la phrase

    Args:
        phrase (str): chaine de caractère 

    Returns:
        list : liste des mots de la phrase
    """    
    liste_mot = []
    mot = ''
    prec = ' '
    for lettre in phrase:
        if lettre.isalpha():
            mot = mot + lettre
        if not lettre.isalpha() and prec.isalpha():
            liste_mot.append(mot)
            mot = ''
        prec = mot
    if len(phrase) != 0:
        if phrase[-1].isalpha():
            liste_mot.append(mot)
    return liste_mot

def test_decoupage_mots():
    assert decoupage_mots("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!") == ["Cela","fait","déjà","jours","jours","à","l","IUT","O","Cool"]
    assert decoupage_mots("") == []
    assert decoupage_mots("Nous devons reussir ce programme") == ["Nous","devons","reussir","ce","programme"]
    assert decoupage_mots("54*1-6") == []

test_decoupage_mots()


#Exercice 6

def recherche_mots_phrase(phrase,lettre):
    """Trouve les mots commençant par une certaine lettre dans une phrase

    Args:
        phrase (str): chaine de caractère 
        lettre (str): lettre choisi
        liste (list): liste de mots

    Return :
        str : liste des mots commençant par une certaine lettre dans une phrase
    """    
    liste_mots = decoupage_mots(phrase)
    return rechercher_mots(liste_mots,lettre)

def test_recherche_mots_phrase():
    assert recherche_mots_phrase("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!","C") == ["Cela","Cool"]
    assert recherche_mots_phrase("","a") == []
    assert recherche_mots_phrase("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!","") == []
    assert recherche_mots_phrase("","") == []
    assert recherche_mots_phrase("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!","V") == []
    assert recherche_mots_phrase("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!","f") == ["fait"]

test_recherche_mots_phrase()

#Exercice 7.1

def init_bool(nb):
    """ Retourne une liste de bool intitalisé à True sauf les deux premiers

    Args:
        nb(int): liste de booléens
    Return :
        list : retourne une liste de bool
    """ 
    liste_bool = []
    for nombre in range(nb+1):
        if nombre == 0 or nombre == 1:
            liste_bool.append(False)
        else:
            liste_bool.append(True)
    return liste_bool

def test_init_bool():
    assert init_bool(5) == [False,False,True,True,True,True]
    assert init_bool(2) == [False,False,True]
    assert init_bool(0) == [False]
    assert init_bool(7) == [False,False,True,True,True,True,True,True]

test_init_bool()

#Exercice 7.2

def bool_nb_premiers(nb,nb_premier):
    """
    oublie de la forme [F,F,T,T,T,...]
    """
    for indice in range(2*nb,len(nb_premier),nb):
        nb_premier[indice] = False
    return nb_premier

def test_bool_nb_premier():
    assert bool_nb_premiers(2, [False,False,True,True,True,True,True]) == [False,False,True,True,False,True,False]
    assert bool_nb_premiers(0, [False,False,True,True,True,True,True]) == [False,False,True,True,True,True,True]