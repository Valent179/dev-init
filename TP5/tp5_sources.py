# Dès que je peux arrêter programme, on le fait
#Exercice 1

from types import resolve_bases


def mystere(liste, valeur):
    """ Retourne le nombre d'éléments parcourus pour avoir au moins 4 éléments
        égal à la valeur

    Args:
        liste (list): liste de nombres
        valeur (int): un nombre entier

    Returns:
        int: nombre de valeur pour trouver 3 élément supérieur à 3
    """
    #cpt est le nombre d'elem déjà énumérés dans la liste
    #cpt_valeur est le nombre d'elem déjà énumérés égal à valeur 
    cpt = 0 
    cpt_valeur = 0
    for elem in range(len(liste)):
        if liste[elem] == valeur:
            cpt_valeur += 1
            if cpt_valeur > 3:#retourne cpt si et seulement si cpt_valeur > 3
                return cpt
        cpt += 1
    return None

def test_mystere():
    assert mystere([12, 5, 8, 48, 12, 418, 185, 17, 5, 87], 20) is None
    assert mystere([12, 5, 20, 48, 12, 418, 20, 17, 5, 20], 20) is None
    assert mystere([], 20) is None
    assert mystere([12, 20, 20, 48, 20, 418, 185, 20, 5, 20], 20) == 7

#Exercice 2.1

def indice_premier_carac_nombre(cdc):
    """ Donne l'indice du premier caractère contenant un chiffre dans une cdc

    Args:
        cdc (str): chaînes de caractères

    Returns:
        int : l'indice du premier caractère contenant un chiffre
    """ 
    for elem in range(len(cdc)): 
        if cdc[elem] in '0123456789':
            return elem
    return None

def test_indice_premier_carac_nombre():
    assert indice_premier_carac_nombre("on est le 30/09/2021") == 10
    assert indice_premier_carac_nombre("nous sommes passé en automne") is None
    assert indice_premier_carac_nombre("10/1310/1515/130") == 0

#Exercice 2.2

def popu_ville(liste_villes,liste_population,nom_ville):
    """
    compléter docstring
    """
    if len(liste_villes) != len(liste_population):
        return None
    for ind in range (len(liste_villes)):
        if liste_villes[ind] == nom_ville:
            return liste_population[ind]
    #la ville n'existe pas 
    return None

def test_popu_ville():
    assert popu_ville(["Blois", "Bourges" ,"Chartres" ,"Châteauroux" ,"Dreux" ,"Joué-lès-Tours" ,"Olivet", "Orléans", "Tours", "Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725],"Orléans") == 116238
    assert (["Blois","Bourges"],[45871, 48965], "Cherveux") is None
    assert (["Blois", "Bourges", "Chartres"], [45871, 64668,  38426], "Orléans") is None


#Exercice 2.2

def popu_ville(liste_villes,liste_population,nom_ville):
    """
    compléter docstring
    """
    if len(liste_villes) != len(liste_population):
        return None
    for ind in range (len(liste_villes)):
        if liste_villes[ind] == nom_ville:
            return liste_population[ind]
    #la ville n'existe pas 
    return None

def test_popu_ville():
    assert popu_ville(["Blois", "Bourges" ,"Chartres" ,"Châteauroux" ,"Dreux" ,"Joué-lès-Tours" ,"Olivet", "Orléans", "Tours", "Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725],"Orléans") == 116238
# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes = ["Blois", "Bourges" ,"Chartres" ,"Châteauroux" ,"Dreux" ,"Joué-lès-Tours" ,"Olivet", "Orléans", "Tours", "Vierzon"]
population = [45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]


#Exercice 3.1

def ordre_croissant_liste(liste):
    """ Donne si une liste est dans l'ordre croissant

    Args:
        liste(list) : liste de nombre 

    Retunrs: 
        bool : si la liste est dans l'ordre croissant
    """
    for ind in range(1,len(liste)):
        if liste[ind] < liste[ind-1]:
            return False
    return True

def test_ordre_croissant_liste():
    assert ordre_croissant_liste([1, 2, 3, 4, 5])
    assert ordre_croissant_liste([])
    assert not ordre_croissant_liste([2,4,6,9,5])
    assert ordre_croissant_liste([-9,-8,-6,-5])

test_ordre_croissant_liste()

#Exercice 3.2

def somme_sup_seuil(liste,valeur):
    """ Donne si la somme dépasse la valeur

    Args:
        liste(list) : liste de nombre
        valeur(int) : valeur du seuil

    Returns:
        bool : retourne si la somme a dépassé le seuil
    """
    somme = 0
    for elem in liste:
        somme += elem
        if somme > valeur:
            return True
    else:
        return False

def test_somme_seuil():
    assert somme_sup_seuil([1, 2 ,3 ,4], 6)
    assert not somme_sup_seuil([-10, 3, 4], 10)
    assert somme_sup_seuil([1, 6, 456, -96], 250)
    assert somme_sup_seuil([-9, -11, -10], -12)

test_ordre_croissant_liste()

#Exercice 3.3

def adresse_mail_espasce(cdc):
    """ Donne si l'adresse e-mail ne contient pas d'espace

    Args:
        cdc(str) : chaîne de caractère

    Returns:
        bool : retourne si cdc contient un espace ou non
    """
    for elem in cdc:
        if elem == " ":
            return False
    return True

def test_adresse_mail_espace():
    assert adresse_mail_espasce("valentin.russeil@gmail.com")
    assert not adresse_mail_espasce("valentin russeil@gmail.com")

def adresse_mail_arobase(cdc):
    """Donne si l'adresse e-mail contient un arobase

    Args:
        cdc (str): chaînes de caractère

    Returns:
        bool: si cdc contient 1 ou plusieurs arobase
    """ 
    arobase = False  
    for elem in cdc:
        if elem == "@":
            if arobase: # vérifie si arobase est égal à true ou false 
                return False # déjà égal à true
            arobase = True
        if elem =="." and arobase: # regarde s'il y a un point après l'arobase
            return True
    return False

def test_adresse_mail_arobase():
    assert adresse_mail_arobase("valentinrusseil@gmail.com") 
    assert not adresse_mail_arobase("valentin@russeiil@gmail.com")
    assert not adresse_mail_arobase("valentin.russeil@gmailcom")
    assert adresse_mail_arobase("valentin.russeil@gmail.com")

test_adresse_mail_arobase()

def adresse_mail_debut_fin(cdc):
    """Donne si un mail se termine par un point ou non

    Args:
        cdc (str): chaines de caractère d'une adresse e-mail

    Returns:
        bool: si l'adresse se termine par un point 
    """ 
    if cdc != "":   
        if cdc[0] == "@":
            return False
        if cdc[-1] == ".":
            return False 
        return True
    return False

def test_adresse_email_debut_fin():
    assert adresse_mail_debut_fin("valentin.russeil@gmail.com")
    assert not adresse_mail_debut_fin("valentinrusseil@gmail.com.")
    assert not adresse_mail_debut_fin("@valentinrusseil@gmail.com")
    assert not adresse_mail_debut_fin("@valentinrusseil@gmail.com.")

test_adresse_email_debut_fin()

def adresse_mail(cdc):
    """Vérifie qu'une chaîne de carac correspond à une adresse e-mail potentielle en respectant toute les contraintes

    Args:
        cdc (str): chaînes de caractère d'une adresse e-mail

    Returns:
        bool: si l'adresse potentielle pourrait être une vrai adresse e-mail
    """    
    if not adresse_mail_debut_fin(cdc):
        return False
    if not adresse_mail_arobase(cdc):
        return False
    if not adresse_mail_espasce(cdc):
        return False
    return True

def test_adresse_mail():
    assert adresse_mail("valentinrusseil@gmail.com")
    assert not adresse_mail("@valentinrusseil@gmail.com")
    assert not adresse_mail("valentin@gmail.com.")
    assert not adresse_mail("valentinrusseil.com")
    assert not adresse_mail("valentin russseil@gmail.com")
    assert not adresse_mail("")

test_adresse_mail()



# Exercice 4.1

def meilleur_score_joueur(scores,joueurs,joueur):
    """Donne le meilleur score d'un joueur 

    Args:
        liste_scores (list): liste de scores
        liste_joueurs (list): liste de noms de joueurs
        joueur (str): nom d'un joueur

    Returns:
        str: meilleur score d'un joueur
    """
    if joueur != '' or joueur not in joueurs:
        for ind in range(len(scores)):
            if joueurs[ind] == joueur:
                return scores[ind]
    return None

def test_meilleur_score_joueur():
    assert meilleur_score_joueur([352100, 325410, 312785, 220199, 127853], ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], 'Batman') == 352100
    assert meilleur_score_joueur([352100, 325410, 312785, 220199, 127853], ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], 'Batgirl') is None
    assert meilleur_score_joueur([352100, 325410, 312785, 220199, 127853], ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], '') is None
    assert meilleur_score_joueur([352100, 325410, 312785, 220199, 127853], ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], 'Superman') is None

test_meilleur_score_joueur()


# Exercice 4.2

def meilleurs_scores_decroissant(liste_scores):
    """Donne si la liste des meilleurs scores est décroissant

    Args:
        liste_scores (list): liste de scores

    Returns:
        bool: si la liste est bien décroissant
    """    
    if liste_scores == []:
        return True
    for ind in range(1,len(liste_scores)):
        if liste_scores[ind] > liste_scores[ind-1]:
            return False
    return True

def test_meilleurs_scores_decroissant():
    assert meilleurs_scores_decroissant([352100, 325410, 312785, 220199, 127853])
    assert meilleurs_scores_decroissant([])
    assert not meilleurs_scores_decroissant([352100, 325410, 312785, 220199, 127853, 1000000])
    assert meilleurs_scores_decroissant([352100, 325410, 312785, 220199, 127853, 123654, 123123])

test_meilleurs_scores_decroissant()

# Exercice 4.3

def nb_joueur_meilleurs_scores(joueurs,joueur):
    """Donne le nombre de fois qu'un joueur est apparu dans les meilleurs scores

    Args:
        joueurs (list): liste des joueurs
        joueur (str): nom d'un score

    Returns:
        int: nb de fois que le joueur appelé est dans la liste
    """    
    cpt = 0
    if joueurs != []:
        for ind in range(len(joueurs)):
            if joueurs[ind] == joueur:
                cpt += 1
    return cpt

def test_nb_joueur_meilleurs_scores():
    assert nb_joueur_meilleurs_scores(joueurs, 'Batman') == 3
    assert nb_joueur_meilleurs_scores(joueurs, 'Robin') == 1
    assert nb_joueur_meilleurs_scores(joueurs, 'Batgirl') == 0
    assert nb_joueur_meilleurs_scores([], 'Batman') == 0

joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']
test_nb_joueur_meilleurs_scores()


# Exercice 4.4

def meilleur_classement_joueur(scores,joueurs,joueur):
    """Donne le meilleur classement d'un joueur 

    Args:
        scores (list): liste de scores des joueurs
        joueurs (list): liste des joueurs
        joueur (str): nom du joueur

    Returns:
        int: meilleur score d'un joueur
    """    
    #supposé trié par ordre décroissant
    for ind in range(len(joueurs)):
        if joueurs[ind] == joueur:
                return scores[ind]
    return None

def test_meilleur_classement_joueur():
    scores = [352100, 325410, 312785, 220199, 127853]
    joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']
    assert meilleur_classement_joueur(scores, joueurs, 'Batman') == 352100
    assert meilleur_classement_joueur(scores, joueurs, 'Robin') == 325410
    assert meilleur_classement_joueur(scores, joueurs, 'Superman') is None
    assert meilleur_classement_joueur(scores, joueurs, 'Joker') == 220199



# Exercice 4.5

def inserer_scores_decroissant(liste_scores,score):
    """Donne l'indice ou le score se sera insérer dans la liste scores

    Args:
        liste_scores (list): liste de scores 
        score (str): nb du score  

    Returns:
        str: indice de l'insertion du score dans la liste score
    """    
    for ind in range(len(liste_scores)):
        if score > liste_scores[ind]:
            return ind
    return len(liste_scores)

def test_inserer_scores_decroissant():
    liste_scores = [352100, 325410, 312785, 220199, 127853]
    assert inserer_scores_decroissant(liste_scores, 314570) == 2
    assert inserer_scores_decroissant([], 231) == 0
    assert inserer_scores_decroissant(liste_scores, 145) == 5
    assert inserer_scores_decroissant(liste_scores, 1000000) == 0

liste_scores = [352100, 325410, 312785, 220199, 127853]

# Exercice 4.6

def inserer_score_joueur(liste_joueurs,liste_scores,joueur,score):
    """Permet d'insérer le nom et le score d'un joueur dans les listes respective

    Args:
        liste_joueurs ([type]): [description]
        liste_scores ([type]): [description]
        joueur ([type]): [description]
        score ([type]): [description]

    """
    indice = indice_insertion(liste_scores, score)
    if indice is None: #insère en fin
        liste_scores.append(score)
        liste_joueurs.append(joueur)
    else:
        liste_scores.insert(indice, score)
        liste_joueurs.insert(indice, joueur)
# pas de return dans cette fonction,
# elle modofie ses paramètres

def test_inserer_score_joueur():
    scores = [352100, 325410, 312785, 220199, 127853]
    joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']
    inserer(scores, joueurs, 326550, 'Joker')   
