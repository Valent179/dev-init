# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
OISEAUX=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
COMPTAGE1=[2,0,5,1,2,0,5,3]
COMPTAGE2=[2,1,3,0,0,3,5,1]
COMPTAGE3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
OBSERVATIONS1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Tourterelle",5), ("Rouge-gorge",3)
            ]

OBSERVATIONS2=[
        ("Merle",2), ("Mésange",1), ("Moineau",3), 
        ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)
            ]

OBSERVATIONS3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Tourterelle",4), ("Rouge-gorge",2)
            ]

# --------------------------------------
# FONCTIONS
# --------------------------------------

# Ex 1.2 
'''
def oiseau_le_plus_observe(liste_observation):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    max_observe = 0
    oiseaux_max = None
    for observation in liste_observation:
        if observation[1] > max_observe:
            max_observe = liste_observation[1]
            oiseaux_max = liste_observation[0]
    return oiseaux_max
'''

# Ex 1.3

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    max_observe = 0
    oiseau_max = None
    for ind in range(len(liste_observations)):
        if liste_observations[ind][1] > max_observe:
            max_observe = liste_observations[ind][1]
            oiseau_max = liste_observations[ind][0]
    return oiseau_max


# Ex 2.1

def recherche_oiseau(liste_oiseaux,nom):
    """ Permet de donner les caractétistique (nom, famille) d'un oiseau à partir se son nom

    Args:
        liste_observations (list): liste d'oiseaux
        nom (str) : nom de l'oiseau

    Returns:
        str: tuple le nom et la famille de l'oiseau
    """    
    for oiseau in liste_oiseaux:
        if oiseau[0] == nom:
            return oiseau
    return None

# Ex 2.2

def recherche_par_famille(liste_famille, nom_famille):
    """Donne tout les oiseaux d'une même famille de la liste

    Args:
        liste_oiseaux (list): liste d'oiseaux
        nom_famille (str): nom de famille d'un oiseau

    Returns:
        str: retourne la liste des oiseaux de la même famille
    """
    res = []
    if liste_famille != []:
        for oiseau in liste_famille:
            if oiseau[1] == nom_famille:
                res.append(oiseau[0])
    return res


#Exercice 3.1

def est_liste_observations(liste_observations):
    """Regarde si la liste d'oiseaux respecte les contraintes énumérées

    Args:
        liste_oiseaux (list): liste des tuples des oiseaux observé

    Returns:
        bool: retourne si la liste respecte bien les contraintes
    """    
    for oiseau in range(len(liste_observations)):
        if liste_observations[oiseau][1] == 0:
            return False
        else:
            if liste_observations[oiseau][0] > liste_observations[oiseau-1][0]:
                return False
    return True

# Exercice 3.2

def max_observations(liste_observations):
    """Donne le plus grand nombre de spécimens observés dans la liste

    Args:
        liste_observations (list): liste de tuple de spécimens observés

    Returns:
        int: donne le plus grand nombre de spécimens observés
    """    
    max_observe = 0
    for elem in liste_observations:
        if elem[1] > max_observe:
            max_observe = elem[1]
    return max_observe

# Exercice 3.3

def moyenne_oiseaux_observes(liste_observations):
    """Donne la moyenne des spécimens oberservés dans la liste
    Args:
        liste_observations (list): liste de tuple de spécimens observés

    Returns:
        float: moyenne des spécimens observés
    """
    cpt = 0
    moy = 0
    somme = 0
    for elem in liste_observations:
        somme += elem[1]
        cpt += 1
    moy = somme / cpt
    return moy

#Exercice 3.4 (revoir cette exercice)

def total_famille(liste_oiseaux, liste_observations, nom_famille):
    """Donne le nombre total de spécimens observés pour une famille d'oiseau à partir liste d'oiseau 
    et d'une liste d'observations

    Args:
        liste_oiseaux (list): liste de tuples
        liste_observations (list): liste de tuples

    Returns:
        int: nombre de spécimens observés pour une famille d'oiseau
    """    
    somme = 0
    seclection_oiseaux = recherche_par_famille
    for observation in liste_observations:
        if observation[0] in seclection_oiseaux:
            somme += observation[1]
    return somme

#Exercice 4.1

def construire_liste_observations(liste_oiseaux, liste_comptage):
    """Donne une liste observation à partir de la liste d'oiseaux et de la liste de comptage

    Args:
        liste_oiseaux (list): liste de tuples d'oiseaux et de leurs espèces
        liste_observations (list): liste de nombres
        
    Returns:
        list: donne la liste d'observation
    """   
    liste_observations = [] 
    if len(liste_oiseaux) != len(liste_comptage):
        return liste_observations
    else:
        for oiseau in range(len(liste_oiseaux)):
            liste_observations.append((liste_oiseaux[oiseau][0], liste_comptage[oiseau]))
    return liste_observations

#Exercice 4.2

def creer_ligne_sup(liste_oiseaux):
    """Donne le nombre de spécimens observés demander à l'utilisateur

    Args:
        liste_oiseaux (list): liste de tuple de spécimens

    Returns:
        liste: donne la liste d'observation
    """    
    liste_observations = []
    for oiseau in liste_oiseaux:
        nombre_oiseau = input("Combien de fois a-tu vu un " + oiseau + " ? ")
        liste_observations.append((oiseau, nombre_oiseau))
    return liste_observations


#Exercice 5.1
