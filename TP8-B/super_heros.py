def moyenne_intelligence(super_heros):
    """Donne la moyenne de l'intelligence de tout les super héros dy dictionnaire

    Args:
        avengers(dict): un dictionnaire modélisant des super héros {nom super héros: (force, intelligence, description)}
        
    Returns:
        float: moyenne intelligence des supers
    """    
    somme_intelligence = 0
    cpt_heros = 0
    for (_, intelligence, _) in super_heros.values():
        somme_intelligence += intelligence
        cpt_heros += 1
    if somme_intelligence != 0:
        return somme_intelligence/cpt_heros
    else:
        return None
    
    
def kikelplusfort(super_heros):
    """ Retourne le nom du plus fort des supers héros

    Args:
        super_heros (dict): [un dictionnaire modélisant des super héros {nom super héros: (force, intelligence, description)}]
    
    Returns:
        str: le nom du personnage le plus fort
    """  
    nom_plus_fort = None  
    plus_fort = 0 
    for (nom_perso, (force, _, _)) in super_heros.items():
        if plus_fort < force:
            plus_fort = force
            nom_plus_fort = nom_perso
    return nom_plus_fort


def combienDeCretins(super_heros):
    """ Donne le nombre de personnage inférieur à la moyenne d'intelligence

    Args:
        super_heros (dict): un dictionnaire modélisant des super héros {nom super héros: (force, intelligence, description)}
        
    Returns:
        int: nombre de personne en dessous de la moyenne
    """    
    cpt_moyenne = 0
    moy_inte = moyenne_intelligence(super_heros)
    for (_, intelligence, _) in super_heros.values():
        if intelligence < moy_inte:
            cpt_moyenne += 1
    return cpt_moyenne