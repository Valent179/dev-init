# TP8 B - Manipuler des listes, ensembles et dictionnaires


def total_animaux(troupeau):
    """ Calcule le nombre total d'animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        int: le nombre total d'animaux dans le troupeau
    """
    somme_animaux = 0
    for animau in troupeau.values():
        somme_animaux += animau
    return somme_animaux
        


def tous_les_animaux(troupeau):
    """ Détermine l'ensemble des animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        set: l'ensemble des animaux du troupeau
    """
    ensemble_animau = set()
    for animau in troupeau:
        ensemble_animau.add(animau)
    return ensemble_animau


def specialise(troupeau):
    """ Vérifie si le troupeau contient 30 individus ou plus d'un même type d'animal 

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient 30 (ou plus) individus d'un même type d'animal,
        False sinon 
    """
    troupeau_animaux = False
    for animau in troupeau.values():
        if animau >= 30:
            troupeau_animaux = True
    return troupeau_animaux
        


def le_plus_represente(troupeau):
    """ Recherche le nom de l'animal qui a le plus d'individus dans le troupeau
    
    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        str: le nom de l'animal qui a le plus d'individus  dans le troupeau
        None si le troupeau est vide) 
    
    """
    max_animaux = 0
    nom_animal = None
    for (animau, quantite) in troupeau.items():
        if max_animaux < quantite:
            max_animaux = quantite
            nom_animal = animau
    return nom_animal


def quantite_suffisante(troupeau):
    """ Vérifie si le troupeau contient au moins 5 individus de chaque type d'animal

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient au moins 5 individus de chaque type d'animal
        False sinon    
    """
    quantite_animaux = True
    for animau in troupeau.values():
        if animau < 5:
            quantite_animaux = False
    return quantite_animaux


def reunion_troupeaux(troupeau1, troupeau2):
    """ Simule la réunion de deux troupeaux

    Args:
        troupeau1 (dict): un dictionnaire modélisant un premier troupeau {nom_animaux: nombre}
        troupeau2 (dict): un dictionnaire modélisant un deuxième troupeau        

    Returns:
        dict: le dictionnaire modélisant la réunion des deux troupeaux    
    """
    reunion_animaux = dict()
    for (animau, quantite) in troupeau1.items():
        reunion_animaux[animau] = quantite
    for (animau, quantite) in troupeau2.items():
        if animau in reunion_animaux:
            reunion_animaux[animau] += quantite
        else:
            reunion_animaux[animau] = quantite
    return reunion_animaux


