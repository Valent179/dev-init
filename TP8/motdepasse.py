# Codé par Papy Force X, jeune padawan de l'informatique
"""
def dialogue_mot_de_passe():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct : 
        mot_de_passe = input("Entrez votre mot de passe : ")
        # je vérifie la longueur
        if len(mot_de_passe) < 8:
            longueur_ok = False;
        else:
            longueur_ok = True
        # je vérifie s'il y a un chiffre
        chiffre_ok = False
        for lettre in mot_de_passe:
            if lettre.isdigit():
                chiffre_ok = True
        # je vérifie qu'il n'y a pas d'espace
        sans_espace = True
        for lettre in mot_de_passe:
            if lettre == " ":
                sans_espace = False
        # Je gère l'affichage
        if not longueur_ok:
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre_ok:
            print("Votre mot de passe doit comporter au moins un chiffre")
        elif not sans_espace:
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe

dialogue_mot_de_passe()
"""

#Ex 1.2
def longueur_ok(mdp):
    """Nous dit si la longueur du mot de passe est correcte

    Args:
        mdp (str): chaîne de caractère du mot de passe
    """
    return len(mdp) >= 8

def chiffre_ok(mdp):
    """Nous dit si le mot de passe contient un chiffre

    Args:
        mdp (str): chaîne de caractère du mot de passe
    """    
    ind = 0
    chiffre = False
    while ind < len(mdp) and not chiffre:
        if mdp[ind].isdigit():
            chiffre = True 
        else: ind += 1
    return chiffre

def sans_espace(mdp):
    """Nous dit si le mot de passe ne contient pas d'espace

    Args:
        mdp (str): chaîne de caractère du mot de passe
    """
    espace = True
    ind = 0
    while ind < len(mdp) and espace:
        if mdp[ind] == " ":
            espace = False
        else:
            ind += 1
    return espace


def dialogue_mot_de_passe():
    """Nous dit si le mot de passe est correcte
    """
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    mot_de_passe = input("Entrez votre mot de passe : ")
    if longueur_ok(mot_de_passe):
        if chiffre_ok(mot_de_passe):
            if sans_espace(mot_de_passe):
                mot_de_passe_correct = True
                print("Votre mot de passe est correcte")
            else:
                print("Il y a un espace dans votre mot de passe")
        else:
            print("Il n'y a pas de chiffre dans votre mot de passe")
    else:
        print("Votre mot de passe n'est pas correcte.")
    return mot_de_passe

#dialogue_mot_de_passe()


#Ex 2
def chiffre_ok_3(mdp):
    """Nous dit si le mot de passe contient 3 chiffres

    Args:
        mdp (str): chaîne de caractère du mot de passe
    """    
    ind = 0
    nb_chiffre = 0
    while ind < len(mdp) and nb_chiffre < 3:
        if mdp[ind].isdigit():
            nb_chiffre += 1
        else:
            ind += 1
    return nb_chiffre


def chiffre_non_consecutifs(mdp):
    """Nous dit si le mot de passe n'a pas de chiffre consécutif

    Args:
        mdp (str): chaine de caractère du mot de passe
    """    
    ind = 1
    non_consecutif = True
    while ind < len(mdp) and non_consecutif:
        if mdp[ind].isdigit():
            if mdp[ind-1].isdigit():
                non_consecutif = False
            else:
                ind += 1
        else:
            ind += 1
    return non_consecutif


def chiffre_plus_petit_apparet_une_fois(mdp):
    """[summary]

    Args:
        mdp ([type]): [description]
    """   
     