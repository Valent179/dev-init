# pylint: disable=missing-function-docstring
"""les tests pour les foctions des exercices 1, 3 et 4 du TP9"""
import tp9

# ==================================
# TESTS pour l'exercice 1
# ==================================

def test_toutes_les_familles():
    mon_pokedex = [('Bulbizarre', 'Plante'), ('Aeromite', 'Poison'), ('Abo', 'Poison')]
    assert tp9.toutes_les_familles(mon_pokedex) == {'Plante', 'Poison'}


def test_nombre_pokemons():
    mon_pokedex = [('Bulbizarre', 'Plante'), ('Aeromite', 'Poison'), ('Abo', 'Poison')]
    assert tp9.nombre_pokemons(mon_pokedex, 'Plante') == 1
    assert tp9.nombre_pokemons(mon_pokedex, 'Poison') == 2
    assert tp9.nombre_pokemons(mon_pokedex, 'Insecte') == 0


def test_frequences_famille():
    mon_pokedex = [('Bulbizarre', 'Plante'), ('Aeromite', 'Poison'), ('Abo', 'Poison')]
    assert tp9.frequences_famille(mon_pokedex) == {'Plante': 1, 'Poison': 2}


def test_dico_par_famille():
    mon_pokedex = [('Bulbizarre', 'Plante'), ('Aeromite', 'Poison'), ('Abo', 'Poison')]
    assert tp9.dico_par_famille(mon_pokedex) == {
        'Plante': {'Bulbizarre'},
        'Poison': {'Aeromite', 'Abo'}}
test_dico_par_famille()


def test_famille_la_plus_representee():
    mon_pokedex = [('Bulbizarre', 'Plante'), ('Aeromite', 'Poison'), ('Abo', 'Poison')]
    assert tp9.famille_la_plus_representee(mon_pokedex) == 'Poison'


# ==================================
# TESTS pour l'exercice 3
# ==================================
"""
mqrf3 = {"Abribus":"Astus", "Jeancloddus":None ,
"Plexus":"Jeancloddus", "Astus":"Gugus",
"Gugus":"Plexus", "Saudepus":"Bielorus"}
"""


def test_quel_guichet():
    mqrf1 = {"Abribus":"Astus", "Jeancloddus":"Abribus", "Plexus":"Gugus",
             "Astus":None, "Gugus":"Plexus", "Saudepus":None}
    mqrf2 = {"Abribus":"Astus", "Jeancloddus":None, "Plexus":"Saudepus",
             "Astus":"Gugus", "Gugus":"Plexus", "Saudepus":None}
    assert tp9.quel_guichet(mqrf1, "Saudepus") == "Saudepus"
    assert tp9.quel_guichet(mqrf2, "Abribus") == "Saudepus"


def test_quel_guichet_v2():
    mqrf1 = {"Abribus":"Astus", "Jeancloddus":"Abribus", "Plexus":"Gugus",
             "Astus":None, "Gugus":"Plexus", "Saudepus":None}
    mqrf2 = {"Abribus":"Astus", "Jeancloddus":None, "Plexus":"Jeancloddus",
             "Astus":"Gugus", "Gugus":"Plexus", "Saudepus":"Bielorus"}
    assert tp9.quel_guichet_v2(mqrf1, "Saudepus") == ("Saudepus", 1)
    assert tp9.quel_guichet_v2(mqrf2, "Abribus") == ("Jeancloddus", 5)


def test_quel_guichet_v3():
    mqrf1 = {"Abribus":"Astus", "Jeancloddus":"Abribus", "Plexus":"Gugus",
             "Astus":None, "Gugus":"Plexus", "Saudepus":None}
    assert tp9.quel_guichet_v3(mqrf1, "Abribus") == ("Astus", 2)
    assert tp9.quel_guichet_v3(mqrf1, "Plexus") is None


# ==================================
# TESTS pour l'exercice 4
# ==================================

def test_toutes_les_familles_v2():
    mon_pokedex = {"Bulbizarre":{"Plante", "Poison"},
                   "Aeromite":{"Poison", "Insecte"}, "Abo":{"Poison"}}
    assert tp9.toutes_les_familles_v2(mon_pokedex) == {'Plante', 'Insecte', 'Poison'}


def test_nombre_pokemons_v2():
    mon_pokedex = {"Bulbizarre":{"Plante", "Poison"},
                   "Aeromite":{"Poison", "Insecte"}, "Abo":{"Poison"}}
    assert tp9.nombre_pokemons_v2(mon_pokedex, 'Plante') == 1
    assert tp9.nombre_pokemons_v2(mon_pokedex, 'Poison') == 3
    assert tp9.nombre_pokemons_v2(mon_pokedex, 'Fée') == 0


def test_frequences_famille_v2():
    mon_pokedex = {"Bulbizarre":{"Plante", "Poison"},
                   "Aeromite":{"Poison", "Insecte"}, "Abo":{"Poison"}}
    assert tp9.frequences_famille_v2(mon_pokedex) == {'Plante': 1, 'Poison': 3, 'Insecte':1}


def test_dico_par_famille_v2():
    mon_pokedex = {"Bulbizarre":{"Plante", "Poison"},
                   "Aeromite":{"Poison", "Insecte"}, "Abo":{"Poison"}}
    assert tp9.dico_par_famille_v2(mon_pokedex) == {
        'Plante': {'Bulbizarre'},
        'Poison': {'Aeromite', 'Abo', 'Bulbizarre'},
        'Insecte':{'Aeromite'}}


def test_famille_la_plus_representee_v2():
    mon_pokedex = {"Bulbizarre":{"Plante", "Poison"},
                   "Aeromite":{"Poison", "Insecte"}, "Abo":{"Poison"}}
    assert tp9.famille_la_plus_representee_v2(mon_pokedex) == 'Poison'
