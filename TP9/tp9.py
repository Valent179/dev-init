"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

def toutes_les_familles(pokedex): # Compléxité : O(N)
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    familles_pok = set()
    for (_, nom_famille) in pokedex:
        if nom_famille not in pokedex:
            familles_pok.add(nom_famille)
    return familles_pok

def nombre_pokemons(pokedex, famille): # Compléxité O(N)
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    cpt_famille = 0
    for (_, nom_famille) in pokedex:
        if nom_famille == famille:
            cpt_famille += 1
    return cpt_famille


def frequences_famille(pokedex): # Compléxité O(N)
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    dict_famille = dict()
    for (_, nom_famille) in pokedex:
        if nom_famille in dict_famille:
            dict_famille[nom_famille] += 1
        else:
            dict_famille[nom_famille] = 1
    return dict_famille

def dico_par_famille(pokedex): # Compléxité O(N)
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dict_famille_pokemon = dict()
    for (pokemon, nom_famille) in pokedex:
        if nom_famille not in dict_famille_pokemon:
            dict_famille_pokemon[nom_famille] = set()
        dict_famille_pokemon[nom_famille].add(pokemon)
    return dict_famille_pokemon

def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    dict_famille = frequences_famille(pokedex)
    plus_representer = None
    nb_plus_representer = 0
    for (nom_famille, nombre) in dict_famille.items():
        if nb_plus_representer is None or nombre > nb_plus_representer:
            nb_plus_representer = nombre
            plus_representer = nom_famille
    return plus_representer
            


# ==========================
# La maison qui rend fou
# ==========================

def quel_guichet(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        str: le nom du guichet qui finit par donner le formulaire A-38
    """
    while not mqrf[guichet] is None:
        guichet = mqrf[guichet]
    return guichet


def quel_guichet_v2(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
    """
    cpt_visite_guichet = 1
    while not mqrf[guichet] is None:
        guichet = mqrf[guichet]
        cpt_visite_guichet += 1
    return (guichet, cpt_visite_guichet)


def quel_guichet_v3(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
        S'il n'est pas possible d'obtenir le formulaire en partant du guichet de depart,
        cette fonction renvoie None
    """
    cpt_visite_guichet = 1
    while cpt_visite_guichet < len(mqrf) and not mqrf[guichet] is None:
        guichet = mqrf[guichet]
        cpt_visite_guichet += 1
    if cpt_visite_guichet >= len(mqrf):
        return None
    else: 
        return (guichet, cpt_visite_guichet)


# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    pokemon = set()
    for (_, type) in pokedex.items():
        for nom_type in type:
            if nom_type not in pokemon:
                pokemon.add(nom_type)
    return pokemon

def nombre_pokemons_v2(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    nb_pokemon = 0
    for (_, type) in pokedex.items():
        for nb_type in type:
            if nb_type == famille:
                nb_pokemon += 1
    return nb_pokemon

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    dict_famille = dict()
    for (_, type) in pokedex.items():
        for nom_type in type:    
            if nom_type not in dict_famille:
                dict_famille[nom_type] = 0
            dict_famille[nom_type] += 1
    return dict_famille

def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dict_famille_pokemon = dict()
    for (pokemon, type) in pokedex.items():
        for nom_type in type:
            if nom_type not in dict_famille_pokemon:
                dict_famille_pokemon[nom_type] = set()
            dict_famille_pokemon[nom_type].add(pokemon)
    return dict_famille_pokemon


def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    nb_famille_plus_representer = 0
    famille_plus_representer = None
    dico_famille = frequences_famille_v2(pokedex)
    for (type, nombre_representation) in dico_famille.items():
        if nb_famille_plus_representer is None or nombre_representation > nb_famille_plus_representer:
            nb_famille_plus_representer = nombre_representation
            famille_plus_representer = type
    return famille_plus_representer