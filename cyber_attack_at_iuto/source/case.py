"""
              Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module case.py
    ce module gère les cases du plateau
"""
import trojan
import protection


def creer_case(fleche, la_protection, serveur, liste_trojans):
    """créer une case du plateau

    Args:
        fleche (str): une des quatre directions 'H' 'B' 'G' 'D' ou '' si pas de flèche sur la case
        la_protection (dict): l'objet de protection posé sur la case (None si pas d'œobjet)
        serveur (dict): le serveur posé sur la case (None si pas de serveur)
        liste_trojan (list): la liste des trojans présents sur le case

    Returns:
        dict: la représentation d'une case
    """
    return {'fleche': fleche, 'la_protection': la_protection, 'serveur': serveur, 
            'liste_trojans_entrants': [], 'liste_trojans': liste_trojans, 'avatar': 0}


def get_fleche(case):
    """retourne la direction de la flèche de la case

    Args:
        case (dict): une case

    Returns:
        str: la direction 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """
    return case['fleche']


def get_protection(case):
    """retourne l'objet de protection qui se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        dict: l'objet de protection présent sur la case (None si pas d'objet)
    """
    return case['la_protection']


def get_serveur(case):
    """retourne le serveur qui se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        dict: le serveur présent sur la case (None si pas d'objet)
    """
    return case['serveur']


def get_trojans(case):
    """retourne la liste des trojans présents sur la case

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans présents sur la case
    """
    return case['liste_trojans']


def get_trojans_entrants(case):
    """retourne la liste des trojans qui vont arriver sur la case

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans qui vont arriver sur la case
    """
    return case['liste_trojans_entrants']


def set_fleche(case, direction):
    """affecte une direction à la case

    Args:
        case (dict): une case
        direction (dict): 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """
    case['fleche'] = direction


def set_serveur(case, serveur):
    """affecte un serveur à la case

    Args:
        case (dict): une case
        serveur (str): le serveur
    """
    case['serveur'] = serveur


def set_protection(case, la_protection):
    """affecte une protection à la case

    Args:
        case (dict): une case
        la_protection (dict): la protection
    """
    case['la_protection'] = la_protection


def set_les_trojans(case, trojans_presents, trojans_entrants):
    """fixe la liste des trojans présents et les trojans arrivant sur la case

    Args:
        case (dict): une case
        trojans_presents (list): une liste de trojans
        trojans_entrants ([type]): une liste de trojans
    """
    case['liste_trojans'] = trojans_presents
    case['liste_trojans_entrants'] = trojans_entrants


def ajouter_trojan(case, un_trojan):
    """ajouter un nouveau trojan arrivant à une case

    Args:
        case (dict): la case
        un_trojan (dict): le trojan à ajouter
    """
    case['liste_trojans_entrants'].append(un_trojan)

def mettre_a_jour_case(case):
    """met les trojans arrivants comme présents et réinitialise les trojans arrivants
       change la direction du trojan si nécessaire (si la case comporte une flèche)
       la fonction retourne un dictionnaire qui indique pour chaque numéro de joueur
       le nombre de trojans qui vient d'arriver sur la...
     case.
       La fonction enlève une resistance à protection qui se trouve sur elle et la détruit si
       la protection est arrivée à 0.

    Args:
        case (dict): la case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs
              le nombre de trojans arrivés sur la case pour ce joueur
    """
    set_les_trojans(case, get_trojans_entrants(case), [])
    dict_troj = dict()
    # on va regarder pour chaque trojans sur la case
    for troj in get_trojans(case):
        # on regarde si la case à une flèche ou pas
        if get_fleche(case) == '':
            trojan.set_direction(case, troj)
        # regarde si le trojan est dans le dictionnaire ou pas     
        if trojan.get_createur(troj) not in dict_troj:
            dict_troj['troj'] = 0
        dict_troj['troj'] += 1
        # regarde si la protection est différent de PAS_DE_PROTECTION et on enlève un à la résistance
        if protection.get_type(get_protection(case)) != protection.PAS_DE_PROTECTION:
            protection.enlever_resistance(get_protection(case))
        # regarde s'il y a encore la résistance et si non on enlève la protection
        if protection.enlever_resistance(get_protection(case)) == 0:
            protection.creer_protection(5)
    return dict_troj


def poser_avatar(case):
    """pose l'avatar sur cette case ec1 = case1()
    c2 = case2()
    c3 = case3()
    c4 = case4()t élimines les trojans présents sur la case.
       la fonction indique combien de trojans ont été éliminés

    Args:
        case (dict): une case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs le nombre
        de trojans éliminés pour ce joueur.
    """
    # La valeur de avatar quand on le pose sera de 1
    case['avatar'] = 1
    dico_joueurs = {1:0, 2:0, 3:0, 4:0}
    # regarde pour chaque trojan de la case
    for troj in get_trojans(case):
        # regarde si la case contient un avatar
        if contient_avatar(case):
            # si le trojan est dans le dictionnaire ou pas
            dico_joueurs[trojan.get_createur(troj)] += 1
    # rénitialise les trojans présent et les trojans entrants
    set_les_trojans(case, [], get_trojans_entrants(case))
    return dico_joueurs
        


def enlever_avatar(case):
    """enlève l'avatar de la case

    Args:
        case (dict): une case
    """
    # La valeur de avatar quand on l'enlève sera de 0
    case['avatar'] = 0


def contient_avatar(case):
    """vérifie si l'avatar se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        bool: True si l'avatar est sur la case et False sinon[type]: [description]
    """
    if case['avatar'] == 1  :
        return True
    return False


def reinit_trojans_entrants(case):
    """reinitialise la liste des trojans entrants à la liste vide

    Args:
        case (dict): une case
    """
    case['liste_trojans_entrants'] = []