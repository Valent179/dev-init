"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


from operator import truediv


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    matrice = dict()
    for val in range(nb_lig):
        for val2 in range(nb_col):
            matrice[(val, val2)] = val_defaut
    return matrice


def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    max_ligne = None
    for (lig, _) in matrice.keys():
        if max_ligne is None or lig > max_ligne:
            max_ligne = lig
    return max_ligne + 1


def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    max_colonne = None
    for (_, col) in matrice.keys():
        if max_colonne is None or col > max_colonne:
            max_colonne = col
    return max_colonne + 1


def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """
    return matrice[(lig, col)]


def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    matrice[(lig, col)] = val


def max_matrice(matrice, interdits=None):
    """retourne la liste des coordonnées des cases contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmi les interdits.

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to None

    Returns:
        list: la liste des coordonnées de cases de valeur maximale dans la matrice (hors cases interdites)
    """
    max_valeur = None
    liste_coordonner = []
    #on parcours toute les coordonnées de la matrice
    for ((ligne, colonne), valeur) in matrice.items():
        # on regarde si valeur est à None ou que la coordonnée prit ne soit pas dans l'ensemble des interdits
        if interdits is None or (ligne, colonne) not in interdits:
            # on regarde si la valeur des coordonnées est supérieur au max actuellement trouvé 
            if max_valeur is None or valeur > max_valeur:
                # réinitialise la liste et remplace par la nouvelle valeur
                liste_coordonner = [(ligne, colonne)]
                max_valeur = valeur
            else: # si la valeur des coordonnées est égal au max actuel
                if valeur == max_valeur:
                    liste_coordonner.append((ligne, colonne))
    return liste_coordonner


DICO_DIR = {(-1, -1): 'HG', (-1, 0): 'HH', (-1, 1): 'HD', (0, -1): 'GG',
            (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD'}


def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permettent d'aller vers la case voisine de 
       la case (ligne,colonne) la plus grande. Le résultat doit aussi contenir la 
       direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    valeur_max = None
    liste_valeur_max = []
    liste_direction = []
    nb_ligne = get_nb_lignes(matrice)
    nb_colonne = get_nb_colonnes(matrice)

    # on va regarder tout les voisins
    for (lig, col) in DICO_DIR:
        coordonnee_ligne = lig + ligne
        coordonnee_colonne = col + colonne
        # on va regarder si les coordonnées voisins sont dans la matrice
        if coordonnee_ligne < nb_ligne and coordonnee_ligne >= 0:
            if coordonnee_colonne < nb_colonne and coordonnee_colonne >= 0:
                valeur = get_val(matrice, coordonnee_ligne, coordonnee_colonne)
                # si la valeur de case voisin est égal à la valeur max
                if valeur_max is None or valeur == valeur_max:
                    liste_valeur_max.append((lig, col))
                    valeur_max = valeur
                else: # si la valeur est différente de la valeur
                    # si la valeur est supérieur à la valeur max 
                    if valeur > valeur_max:
                        liste_valeur_max = []
                        valeur_max = valeur
                        liste_valeur_max.append((lig, col))

    mileux = (2, 2)
    vertical = 0 
    horizontal = 0
    # regarde si les coordonnées placer en paramètre sont pas au milieu
    if (ligne, colonne) != mileux:
        if ligne > 2:
            vertical -= 1
        else:
            if ligne < 2:
                vertical += 1
        if colonne > 2:
            horizontal -= 1
        else:
            if colonne < 2:
                horizontal += 1
    
    # on va regarder si les coordonnées pour aller vers le mileu ne se trouve pas déjà
    # dans liste_valeur_max. Ce test va permettre d'éviter les doublons
    if (vertical, horizontal) not in liste_valeur_max and (vertical, horizontal) != (0,0):        
        liste_valeur_max.append((vertical, horizontal))

    # remplacer toutes les coordonnées(les nombres) par les lettres du dictionnaires
    if liste_valeur_max != []:
        for direction in liste_valeur_max:
            liste_direction.append(DICO_DIR[direction])
    
    return liste_direction