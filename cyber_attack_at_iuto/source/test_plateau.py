# pylint: disable=missing-function-docstring

"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module de tests de plateau.py
"""

import plateau
import matrice
import case
import equipement
import trojan
import protection
import joueur

def plateau1():
    id1 = joueur.creer_joueur(1, 'Zoro', 0)
    plateau1 = plateau.creer_plateau(id1)