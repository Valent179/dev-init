def entiers_consecutifs(liste):
    """ renvoie la plus longue liste de nombre entiers consécutifs

    Args:
        liste (list): liste de nombre non triée
    """    
    liste_triee = sorted(liste)
    plus_long_entiers = None
    entiers_en_cours = [liste_triee[0]]
    for ind in range(1, len(liste_triee)):
        if liste_triee[ind] != liste_triee[ind - 1]:
            if liste_triee[ind] == liste_triee[ind - 1] + 1:
                entiers_en_cours.append(liste_triee[ind])
            else:
                entiers_en_cours = [liste_triee[ind]]
        if plus_long_entiers is None or len(plus_long_entiers) < len(entiers_en_cours):
                plus_long_entiers = entiers_en_cours
    return plus_long_entiers
            
            

def test_entiers_consecutifs():
    assert entiers_consecutifs([7, 1, 14, 11, 2, 17, 6, 15, 2, 8]) == [6, 7, 8]


def ajoute(nombre, liste=triee):
    """[summary]

    Args:
        nombre ([type]): [description]
        liste ([type], optional): [description]. Defaults to triee.
    """    