"""TP7 une application complète
ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
"""
# Exercice 1.1


from os import read


def afficher_menu(titre, liste_options):
    """Affiche un menu à partir du titre et d'une liste_options

    Args:
        titre (str): nom du titre
        liste_options (list): liste d'option
    """
    print("+" + "-"*len(titre) + "+")
    print("|" + titre + "|")
    print("+" + "-"*len(titre) + "+")
    for nom_option in range(1, len(liste_options)+1 ):
        print(str(nom_option) + "-> " + liste_options[nom_option - 1])

def demander_nombre(message, borne_max):
    """Retourne le nombre de l'utilisateur s'il eest compris dans l'intervalle

    Args:
        message (str): message de l'utilisateur
        borne_max (int): nombre max de la borne
    """
    res = input(message)
    if res.isdecimal():
        res = int(res)
        if res >= 1 and res <= borne_max:
            return res
    return None

# Exercice 1.3


def menu(titre, liste_options):
    """Donne le numéro de l'option menu choisie par l'utilisateur

    Args:
        titre (str): titre du menu
        liste_options ([type]): liste d'option du menu
    """
    afficher_menu(titre, liste_options)
    res = demander_nombre("Quelle est votre nombre ? ", len(liste_options))
    return res

# Exercice 1.4


def programme_principal():
    liste_options = ["Charger un fichier", "Rechercher la population d'une commune",
                     "Afficher la population d'un département", "Quitter"]
    liste_communes = []
    while True:
        rep = menu("MENU DE MON APPLICATION", liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            print("Vous avez choisi", liste_options[rep-1])
            chemin_fic = input ( "Quelle est le chemin du fichier que vous voulez charger ? ")
            liste_communes = charger_fichier_population(chemin_fic)
            print(" Vous avez choisi ce chemin " + str(len(liste_communes)))
        elif rep == 2:
            print("Vous avez choisi", liste_options[rep-1])
            commune = input("Quelle est le nom de votre commune ? ")
            lire_commune = population_d_une_commune(liste_communes,commune)
            if lire_commune is None:
               print("Il n'y a pas cette commune dans ce fichier")
            else:
                print("Le nombre d'habitants de cette commune est de ", str(lire_commune)) 
        elif rep == 3:
            print("Vous avez choisi", liste_options[rep-1])
            num_dpt = input("Quelle département voulez-vous choisir ? ")
            lire_commune_plus_peuple = commune_plus_peuplee_departement(liste_communes, num_dpt)
            if lire_commune_plus_peuple is None:
                print("Il n'y a pas de commune dans le département choise de notre liste.")
            else:
                print(" Le commune qui a le plus d'habitants dans le département choisi est ", str(lire_commune_plus_peuple))
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")

#Ex 2.1
def charger_fichier_population(nom_fic):
    liste_fic = []
    fic = open(nom_fic, "r")
    fic.readline()
    for ligne in fic:
        #on apprend un tuple crée par compréhension 
        ligne_fic = ligne.split(";")
        liste_fic.append((ligne_fic[0], ligne_fic[1], ligne_fic[4]))
    fic.close()
    return liste_fic

#Ex 2.3
def population_d_une_commune(liste_pop, nom_commune):
    for ville in liste_pop:
        if ville[1] == nom_commune:
            return ville[2]
    return None

#Ex 2.4
def liste_des_communes_commencant_par(liste_pop, debut_nom):
    liste_commune = []
    for commune in liste_pop:
        if commune[1][0:len(debut_nom)] == debut_nom:
            liste_commune.append(commune[1])
    return liste_commune


#Ex 2.5
def commune_plus_peuplee_departement(liste_pop, num_dpt):
    commune_peuple = 0
    nom_commune_plus_peuple = None
    for commune in range(len(liste_pop)):
        if liste_pop[commune][0].startswith(num_dpt):
            if int(liste_pop[commune][2]) > commune_peuple:
                commune_peuple = int(liste_pop[commune][2])
                nom_commune_plus_peuple = liste_pop[commune][1]
    return nom_commune_plus_peuple
                

#Ex 2.6
def nombre_de_communes_tranche_pop(liste_pop, pop_min, pop_max):
    cpt_commune = 0
    for commune in range(len(liste_pop)):
        if int(liste_pop[commune][2]) >= pop_min and int(liste_pop[commune][2]) <= pop_max:
            cpt_commune += 1
    return cpt_commune


def place_top(commune, liste_pop):
    ...


#Ex 2.7
def ajouter_trier(commune, liste_pop, taille_max):
    for ind in range(len(liste_pop)):
        if commune[2] > liste_pop[ind][2]:
            liste_pop.append((commune[ind]))
    if len(liste_pop) >= taille_max:
        del liste_pop[len(liste_pop)- 1]


def top_n_population(liste_pop, nb):
    ...


def population_par_departement(liste_pop):
    ...


def sauve_population_dpt(nom_fic, liste_pop_dep):
    ...


# appel au programme principal
programme_principal()


#debut_nom = input("Quelle sont les trois initiales de votre commune ? ")
            #lire_init_commune = liste_des_communes_commencant_par(liste_communes, debut_nom)
            #if lire_init_commune == []:
            #    print("Il n'yliste_pop
            #    print("Voici la liste de commune qui commence par ces 3 initiales", str(lire_init_commune))